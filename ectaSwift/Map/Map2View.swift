//
//  Map2View.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 04/12/2020.
//

import MapKit
import SwiftUI

struct MapView2: UIViewRepresentable {
  
  var locationManager = CLLocationManager()
  func setupManager() {
    locationManager.desiredAccuracy = kCLLocationAccuracyBest
    locationManager.requestWhenInUseAuthorization()
    locationManager.requestAlwaysAuthorization()
  }
  
  func makeUIView(context: Context) -> MKMapView {
    setupManager()
    let mapView = MKMapView(frame: UIScreen.main.bounds)
    mapView.showsUserLocation = true
    mapView.userTrackingMode = .follow
    return mapView
  }
  
  func updateUIView(_ uiView: MKMapView, context: Context) {
  }
}

struct Map2View: View {
    var body: some View {
        ZStack {
                VStack {
                    MapView2()
                        .edgesIgnoringSafeArea(.all)
                    Spacer()
                }
        }
    }
}

struct Map2View_Previews: PreviewProvider {
    static var previews: some View {
        Map2View()
    }
}
