//
//  MapView.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 04/12/2020.
//

import MapKit
import SwiftUI

struct MapView: View {
    
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 51.507222, longitude: -0.1275), span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
    
    var body: some View {
        ZStack {
            VStack {
                Map(coordinateRegion: $region)
                    .edgesIgnoringSafeArea(.all)
                Spacer()
            }
        }
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
