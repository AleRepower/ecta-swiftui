//
//  ectaSwiftApp.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 27/11/2020.
//  Copyright © 2020 Repower. All rights reserved.

import SwiftUI

@main
struct ectaSwiftApp: App {

    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
//                .environmentObject(StrutturaWrapper())
        }
    }
}
