//
//  Utilities.swift
//  ectaSwift
//
//  Created by Angelo D'Ariano on 17/08/14.
//  Copyright (c) 2014 Angelo D'Ariano. All rights reserved.
//

import SwiftUI
import CoreLocation


class Utilities {
    

    private(set) var cache = [Set<String.SubSequence>: Int]()
    
    func calculateDistanceLevenshtein(a: String.SubSequence, b: String.SubSequence) -> Int {
        let key = Set([a, b])
        if let distance = cache[key] {
            return distance
        } else {
            let distance: Int = {
                if a.count == 0 || b.count == 0 {
                    return abs(a.count - b.count)
                } else if a.first == b.first {
                    return calculateDistanceLevenshtein(a: a[a.index(after: a.startIndex)...], b: b[b.index(after: b.startIndex)...])
                } else {
                    let add = calculateDistanceLevenshtein(a: a, b: b[b.index(after: b.startIndex)...])
                    let replace = calculateDistanceLevenshtein(a: a[a.index(after: a.startIndex)...], b: b[b.index(after: b.startIndex)...])
                    let delete = calculateDistanceLevenshtein(a: a[a.index(after: a.startIndex)...], b: b)
                    return min(add, replace, delete) + 1
                }
            }()
            cache[key] = distance
            return distance
        }
    }

    
    func checkIOSVersion(_ ref : String) -> (isGreater: Bool, desc: String) {
        let sys = UIDevice.current.systemVersion
        switch sys.compare(ref, options: NSString.CompareOptions.numeric, range: nil, locale: nil) {
        case .orderedAscending:
            return (true, "\(ref) is greater than \(sys)")
        case .orderedDescending:
            return (false, "\(ref) is less than \(sys)")
        case .orderedSame:
            return (false, "\(ref) is the same as \(sys)")
        }
    }
    
    func delayDispatchTime(_ seconds: Double = 0) -> DispatchTime {
        let delay = (seconds * Double(NSEC_PER_SEC))
        return DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
    }
    
//    func deleteDataModel() {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let storeURL = appDelegate.applicationDocumentsDirectory.appendingPathComponent(appDelegate.storeFilename)
//        do {
//            try FileManager.default.removeItem(at: storeURL)
//        } catch _ {
//        }
//    }
    
    func cleanCookies() {
        
        let cookieStorage: HTTPCookieStorage = HTTPCookieStorage.shared
        
        if let cookies = cookieStorage.cookies {
            for cookie in cookies {
                cookieStorage.deleteCookie(cookie as HTTPCookie)
            }
        }
        
        UserDefaults.standard.synchronize()
    }
    
    //    func toggleStatusBarVisibilty(_ becameHidden: Bool = !UIApplication.shared.isStatusBarHidden){
    //
    //        UIApplication.shared.setStatusBarHidden(becameHidden, with: UIStatusBarAnimation.fade)
    //    }
    
    func versionApp() -> String? {
        let dictionary = Bundle.main.infoDictionary!
        guard let version = dictionary["CFBundleShortVersionString"] as? String, let build = dictionary["CFBundleVersion"] as? String else { return nil }
        
        var compileDate:Date
        {
            let bundleName = Bundle.main.infoDictionary!["CFBundleName"] as? String ?? "Info.plist"
            if let infoPath = Bundle.main.path(forResource: bundleName, ofType: nil),
                let infoAttr = try? FileManager.default.attributesOfItem(atPath: infoPath),
                let infoDate = infoAttr[FileAttributeKey.creationDate] as? Date
            { return infoDate }
            return Date()
        }
        
        let myFormatter = DateFormatter()
        myFormatter.dateStyle = .short
        let date = myFormatter.string(from: compileDate) // "3/10/76"
        
        return "v \(version).\(build) \(date)"
    }
    
    //    func openWithoginECTA(viewController: UIViewController) {
    /*
    func openWithoginECTA(withExtraInfo extraInfo:[String:String]? = nil) {
        let thisApp = UIApplication.shared
        if let ectaURL = URL(string: "\(ECTA_URL_SCHEME)://"), thisApp.canOpenURL(ectaURL) {
            
            api.ssoToken(params: ["app_target":"ECTA"], extraInfo: extraInfo)
        } else {
            
            //            let goToItunes = {
            //                (action: String?) -> ((UIAlertAction!) -> ()) in
            //                return {
            //                    _ in
            //                    if let ECTA_STORE_URL = ECTA_STORE_URL, thisApp.canOpenURL(ECTA_STORE_URL) {
            //                        thisApp.openURL(ECTA_STORE_URL)
            //                    }
            //                }
            //            }
            //            let alert = UIAlertController(title: "Energia Che Ti App", message: "ECTA non sembra ancora installata. Vuoi scaricarla da App Store ora?", preferredStyle: .alert)
            //            alert.addAction(UIAlertAction(title: "Non ora", style: .cancel, handler: nil))
            //            alert.addAction(UIAlertAction(title: "Perché no?", style: .default, handler: goToItunes(nil)))
            //            vc.present(alert, animated: true, completion: nil)
            
            let alert = CDAlertView(title: "Energia Che Ti App", message: "ECTA non sembra ancora installata. Vuoi scaricarla da App Store ora?", type: .alarm)
            alert.add(action: CDAlertViewAction(title: "Non ora"))
            alert.add(action: CDAlertViewAction(title: "Perché no?", font: nil, textColor: nil, backgroundColor: nil, handler: { action in
                
                if let ECTA_STORE_URL = ECTA_STORE_URL, thisApp.canOpenURL(ECTA_STORE_URL) {
//                    thisApp.openURL(ECTA_STORE_URL)
                    thisApp.open(ECTA_STORE_URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
                return true
            }))
            alert.show()
            
        }
        
    }
    */
    
    // MARK: - Date managing
    
    /*
     var dateFormatters: Array<NSDateFormatter> = []
     func initDateFormatters()
     {
     let dateFormatterStd = NSDateFormatter()
     let dateFormatterStdUS = NSDateFormatter()
     let dateFormatterColon = NSDateFormatter()
     let dateFormatterDot = NSDateFormatter()
     
     dateFormatterStd.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.S"
     dateFormatterStdUS.dateFormat = "yyyy-MM-dd'T'HH.mm.ss.S"
     dateFormatterColon.dateFormat = "dd/MM/yyyy HH:mm:ss"
     dateFormatterDot.dateFormat = "dd/MM/yyyy HH.mm.ss"
     
     self.dateFormatters = [dateFormatterStd, dateFormatterStdUS, dateFormatterColon, dateFormatterDot]
     }
     
     func dateWithString(string: String) -> NSDate?
     {
     if self.dateFormatters.isEmpty {
     self.initDateFormatters()
     }
     
     for formatter in self.dateFormatters {
     var result: NSDate? = formatter.dateFromString(string)
     
     if result != nil {return result}
     }
     
     return nil;
     }
     
     func stringFromDate(date: NSDate) -> String
     {
     let formatter = NSDateFormatter()
     formatter.dateFormat = "dd/MM/yyyy"
     return formatter.stringFromDate(date)
     
     }
     
     func stringTimeFromDate(date: NSDate) -> String
     {
     let formatter = NSDateFormatter()
     formatter.dateFormat = "dd/MM/yyyy hh:mm:ss"
     return formatter.stringFromDate(date)
     
     }
    
    
    func openExternalMaps(lat: Double, lon: Double) {
        let location = CLLocationCoordinate2DMake(lat, lon)
        
        let promptFunction = { () -> Bool in 
            Localide.sharedManager.promptForDirections(toLocation: location, rememberPreference: false, onCompletion: { (usedApp, fromMemory, openedLinkSuccessfully) in
                if fromMemory {
                    print("Localide used \(usedApp) from user's previous choice.")
                } else {
                    print("Localide " + (openedLinkSuccessfully ? "opened" : "failed to open") + " \(usedApp)")
                }
            })
            return true
        }
        
        if Localide.sharedManager.availableMapApps.count == 1 {
            let appName = Localide.sharedManager.availableMapApps[0].appName
            print("Only found 1 available app, opening it directly")

            let alert = CDAlertView(title: "Percorso", message: "Vuoi utilizzare \(appName) per visualizzare il percorso?", type: .notification)
            alert.circleFillColor = UIColor.repowerRed
            alert.titleTextColor = UIColor.repowerRed
            let nevermindAction = CDAlertViewAction(title: "Annulla")
            nevermindAction.buttonTextColor = UIColor.repowerRed
            alert.add(action: nevermindAction)
            
            let doneAction = CDAlertViewAction(title: "Ok", handler: { _ in
                promptFunction()
            })
            
            doneAction.buttonTextColor = UIColor.repowerRed
            doneAction.buttonFont = UIFont.systemFont(ofSize: 17, weight: .bold)
            alert.add(action: doneAction)
            alert.show()
            
        } else {
            _ = promptFunction()
        }
    }
 
     */
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}

extension Utilities { // Global Info
    func getAppInfo() -> (name: String?, version: String?) {
        let displayName = Bundle.main.infoDictionary?["CFBundleDisplayName"] as? String
        let productName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
        let displayNameString = displayName ?? ""
        let appName = displayNameString.isEmpty ? productName : displayName
        let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        return (appName, appVersion)
    }
    
    func getDeviceInfo() -> (model: String?, osFamily: String?, osVersion: String?, uuid: String?) {
        let model = UIDevice.current.model
        let osVersion = UIDevice.current.systemVersion
        
        func getVendorUUID() -> String? {
            //uuid
            let defaults = UserDefaults.standard
            let uuidStored = defaults.string(forKey: USERDEFAULT_KEY_DEVICEVENDORUUID)
            let uuid = uuidStored ?? UIDevice.current.identifierForVendor?.uuidString
            if let uuid = uuid, uuid != uuidStored { defaults.set(uuid, forKey: USERDEFAULT_KEY_DEVICEVENDORUUID) }
            
            return uuid
        }
        let uuid = getVendorUUID()
        
        return (model, "iOS", osVersion, uuid)
    }
    
    func getGeoInfo() -> (latitude: String?, longitude: String?) {
        let locationManager = CLLocationManager()
        let currentLocation = locationManager.location?.coordinate
        
        if let currentLocation = currentLocation {
            return (String(currentLocation.latitude), String(currentLocation.longitude))
        }
        return (nil, nil)
    }
}

// MARK: - Alert Messages
extension Utilities {
    /*
    func showQuestionYesNo(_ textMessage: String, titleMessage: String = "?", typeImage: UIImage? = nil, yesAction: @escaping () -> Void ) {
        let image = typeImage ?? UIImage(named: "ic_help_outline")
        let type = image == nil ? CDAlertViewType.notification : CDAlertViewType.custom(image: image!)
        
        DispatchQueue.main.async(execute: {
            let alert = CDAlertView(title: titleMessage, message: textMessage, type: type)
            
            alert.add(action: CDAlertViewAction(title: "Non ora", textColor: UIColor.repowerGreyText))
            alert.add(action: CDAlertViewAction(title: "Si, cool!", font: nil, textColor: UIColor.repowerRed, backgroundColor: nil, handler: { action -> Bool in
                yesAction()
                return true
            }))
            
            
            self.setRepowerStyle(to: alert).show()
        })
    }
     */
//    func showMessage(_ textMessage: String, titleMessage: String = "Ehi, psss", type: CDAlertViewType = .notification, titleCancelButton: String? = nil) {
//        DispatchQueue.main.async(execute: {
//            let alert = CDAlertView(title: titleMessage, message: textMessage, type: type)
//
//            if let titleCancelButton = titleCancelButton { alert.add(action: CDAlertViewAction(title: titleCancelButton, textColor: UIColor.repowerRed))}
//
//            self.setRepowerStyle(to: alert).show()
//        })
//    }
    
//    func setRepowerStyle(to alert: CDAlertView) -> CDAlertView {
//        alert.tintColor = UIColor.white
//        alert.titleTextColor = UIColor.repowerGreyText
//        alert.messageTextColor = UIColor.repowerGreyText
//        alert.circleFillColor = UIColor.repowerRed
//        return alert
//    }
    
//    func showAlert(_ textMessage: String, titleMessage: String = "Attenzione!", titleCancelButton: String? = nil) {
//        showMessage(textMessage, titleMessage: titleMessage, type: .warning, titleCancelButton: titleCancelButton)
//    }
    
    func showMessage(_ textMessage: String, titleMessage: String = "Ehi, psss", titleCancelButton: String? = nil) {
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: titleMessage, message: textMessage, preferredStyle: .alert)
            
            // Create OK button with action handler
            let ok = UIAlertAction(title: titleCancelButton, style: .default, handler: { (action) -> Void in
                print("Ok button tapped")
             })
            
            //Add OK button to a dialog message
            alert.addAction(ok)
            // Present Alert to
            UIApplication.shared.windows.first!.rootViewController?.present(alert, animated: true, completion: nil)
            
        })
    }
    
    
    func showAlert(_ textMessage: String, titleMessage: String = "Attenzione!", titleCancelButton: String? = nil) {
        showMessage(textMessage, titleMessage: titleMessage, titleCancelButton: titleCancelButton)
    }

}
