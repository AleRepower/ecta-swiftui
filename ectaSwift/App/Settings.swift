//
//  Settings.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 28/11/2020.
//  Copyright © 2020 Repower. All rights reserved.


import SwiftUI


// API ECTA
let ECTA_BASE_PATH = "https://api.ects.repower.com:8001"             // PROD
let ECTA_BASE_PATH_DEV = "https://ecta.test-it.repower.com:8002"     // DEV

//Login/logout
let URL_POST_LOGIN = "/Authentication.svc/Login"
let URL_POST_LOGOUT = "/Authentication.svc/logout/username"

//DataSet Fissi
let URL_GET_COMUNI = "/DataService.svc/datasetcomuni?"
let URL_GET_TIPI_DOCUMENTO = "/DataService.svc/datasettipidocumento?"
let URL_GET_TIPI_ATTIVITA = "/DataService.svc/datasettipiattivita?"
let URL_GET_TIPI_DOCUMENTO_TRASPORTO = "/DataService.svc/datasetcontrattitrasporto"
let URL_GET_DATASET_CHANGES = "/DataService.svc/datasetchanges"
let URL_GET_DATASET_STATI_PAGAMENTO = "/DataService.svc/DataSetStatipagamento?"

//Chiamate Get Dinamiche
let URL_CERCA_AZIENDE = "/DataService.svc/cercaazienda?"
let URL_GET_DATE_AUTOLETTURA = "/DataService.svc/checksendletturegas?"
let URL_GET_INFO_DATI_AUTOLETTURA = "/DataService.svc/getInfoDatiAutolettura?"
let URL_GET_AZIENDA_INFO = "/DataService.svc/getaziendainfo?"
let URL_GET_CALCOLO_IMPOSTE_EE = "/DataService.svc/calcoloimposteee?"
let URL_GET_CALCOLO_IMPOSTE_GAS = "/DataService.svc/calcoloimpostegas?"
let URL_GET_LETTURE = "/DataService.svc/getLetture?"
let URL_SET_AZIENDA_DEFAULT = "/DataService.svc/setaziendadefault?"

//Energia dal cuore verde
let URL_GET_MODULO_CERTIFICAZIONI = "/DataService.svc/getmodulo_certificazioni?"
let URL_GET_MODULO_RICARICA = "/DataService.svc/getmodulo_ricariche?"
let URL_GET_MODULO_VEICOLI = "/DataService.svc/getmodulo_veicoli?"
let URL_GET_MODULO_EFFICIENZA = "/DataService.svc/getmodulo_efficienza?"
let URL_GET_MODULO_ANALISITERMO = "/DataService.svc/getmodulo_analisitermo?"
let URL_GET_MODULO_MONITORAGGIOCONSUMI = "/DataService.svc/getmodulo_monitoraggioconsumi?"
let URL_GET_EFFETTO_ACCESS_URL = "/DataService.svc/geteffettoaccessurl?"
let URL_GET_REM_ACCESS_URL = "/DataService.svc/getremaccessurl?"

let URL_GET_CONSUMI = "/DataService.svc/getconsumi?"
let URL_GET_COSTI = "/DataService.svc/getcosti?"
//let URL_GET_DISTRIBUTORI = "/DataService.svc/getdistributori?"
let URL_GET_PUNTAVANTI = "/DataService.svc/getpuntavanti?"
let URL_GET_ENERGIA_REATTIVA = "/DataService.svc/getenergiareattiva?"

let URL_GET_SERVIZI_EXTRA = "/DataService.svc/getserviziextra?"

//obsoleto, utilizzare URL_GET_FILE let URL_GET_PDF = "/DataService.svc/getpdf?"
let URL_GET_FILE = "/DataService.svc/getfile?"

let URL_GET_PARAM_PAGAMENTO_ELETTRONICO = "/DataService.svc/getIndirizzoPagamentoElettronico?"




//Consulente
let URL_GET_CONSULENTI = "/DataService.svc/getconsulenti?"
let URL_GET_FOTO_CONSULENTE = "/DataService.svc/getfotoconsulente?"

let URL_GET_CONTATORI = "/DataService.svc/getcontatori?"

//Allarmi
let URL_GET_ALLARMI = "/DataService.svc/getAllarmi?"
let URL_SET_ALLARMI = "/DataService.svc/setAllarmi?"

//Chiamate POST
let URL_POST_FATTURE = "/DataService.svc/getfatture?"

//Invio Mail
let URL_POST_CONSULENTE = "/DataService.svc/contattaConsulente"
let URL_POST_SERVIZIO_CLIENTI = "/DataService.svc/contattaServizioClienti"
let URL_POST_SEGNALA_PROBLEMA = "/DataService.svc/segnalaProblema"

//Autolettura
let URL_POST_AUTOLETTURA = "/DataService.svc/sendletturegas"

//Webview
let URL_WV_DEFAULT = "https://energiachetiserve.repower.com/12e7463c847t0694a0/default.asp"
let URL_WV_RESET_PASSWORD = "https://energiachetiserve.repower.com/12e2482c195t9021a2/reset_password.asp"
let URL_WV_PRIVACY = "https://energiachetiserve.repower.com/12e7463c847t0694a0/privacy.asp"
let URL_WV_EE = "https://energiachetiserve.repower.com/12e7463c847t0694a0/prodotto_ee.asp"
let URL_WV_GAS = "https://energiachetiserve.repower.com/12e7463c847t0694a0/prodotto_gas.asp"
let URL_WV_EDCV = "https://energiachetiserve.repower.com/12e7463c847t0694a0/prodotto_ecv.asp"
let URL_WV_SP = "https://energiachetiserve.repower.com/12e7463c847t0694a0/prodotto_sp.asp"
let URL_WV_MOB = "https://energiachetiserve.repower.com/12e7463c847t0694a0/prodotto_mob.asp"
let URL_WV_NOTIFICHE = "https://energiachetiserve.repower.com/12e7463c847t0694a0/notifiche.asp"
let URL_WV_TRASPORTO_EE = "https://energiachetiserve.repower.com/12e7463c847t0694a0/trasporto_ee.asp"
let URL_WV_ONERI_EE = "https://energiachetiserve.repower.com/12e7463c847t0694a0/oneri_ee.asp"
let URL_WV_ASSICURAZIONE_GAS = "https://energiachetiserve.repower.com/12e7463c847t0694a0/copertura_assicurativa_gas.asp"
let URL_WV_MIX_ENERGIA = "https://energiachetiserve.repower.com/12e7463c847t0694a0/mix_energetico.asp"

let URL_INFO_INSOLVENZE = "https://energiachetiserve.repower.com/12e7463c847t0694a0/data/infoinsolvenze.json"

let URL_COSFI = "https://energiachetiserve.repower.com/costi/cosphi/"
let URL_PUNTAVANTI = "https://energiachetiserve.repower.com/PUNtavanti/grafico/"

// WEB PORTO
let URL_WEB_WORKING_DIR_NORMAL = "https://porto.repower.com/home/673534364b6a058d4/pubblica"
let URL_WEB_WORKING_DIR_AGENTS = "https://porto.repower.com/home/673534364b6a058d4/"

let URL_JSON_RICERCHESUGGERITE = "\(URL_WEB_WORKING_DIR_AGENTS)app_assets/RicercheSuggerite.json"

// more menu, elenco
let LARGHEZZA_AREA_SEMPRE_VISIBILE: CGFloat = 10.0 //25.0


let MAP_ADDRESSVIEW_HEIGHT: CGFloat = 44.0

let REQUEST_TIMEOUTINTERVAL: TimeInterval = 20.0

//User defaults
let USERDEFAULT_KEY_RICERCHESUGGERITE = "PlaylistRicercheSuggerite"

let USERDEFAULT_KEY_DEVICEVENDORUUID = "deviceVendorUUID"
let USERDEFAULT_KEY_TOTAZIENDELIBERE = "totAziendeLibere"
let USERDEFAULT_KEY_TOTAZIENDEPRENOTATE = "totAziendePrenotate"
let USERDEFAULT_KEY_TOTAZIENDECLIENTI = "totAziendeClienti"
let USERDEFAULT_KEY_TOTAZIENDEINIBITE = "totAziendeInibite"
let USERDEFAULT_KEY_TOTAZIENDERISERVATE = "totAziendeRiservate"

let HARDLIMIT_RISULTATI_PLAYLIST = 120
let USERDEFAULT_KEY_FILTRITIPOLOGIA = "filtriTipologia"
let USERDEFAULT_KEY_FILTRIINDIRIZZO = "filtriIndirizzo"
let filtriTipologiaDefault = ["LIBERA","PRENOTATA","CLIENTE","RISERVATA","INIBITA"]
let filtriIndirizzoDefault = ["SEDE LEGALE","INDIRIZZO FORNITURA"]

/*
enum TipologiaAzienda: String {
    case Libera = "LIBERA"
    case Prenotata = "PRENOTATA"
    case Cliente = "CLIENTE"
    case Inibita = "INIBITA"
    case Riservata = "RISERVATA"
}

enum TipoIndirizzoAzienda: String {
    case SedeLegale = "SEDE LEGALE"
    case Fornitura = "INDIRIZZO FORNITURA"
}
*/

let USERDEFAULT_KEY_CRONOLOGIAINDIRIZZICERCATI = "cronoSearchedAddress"
let USERDEFAULT_KEY_CRONOLOGIAPREFILTRICERCATI = "cronoSearchedPlaylistPrefiltri"

//ECTA
let DEVICE_VENDOR_UUID = "deviceVendorUUID"
let ECTA_URL_SCHEME = "comrepowerecta-logintoken"
let ECTA_ITUNES_URL = URL(string: "https://geo.itunes.apple.com/it/app/energia-che-ti-app-ecta/id961642733?mt=8")
let ECTA_STORE_URL = URL(string: "itms://itunes.apple.com/it/app/energia-che-ti-app-ecta/id961642733?mt=8")

//NET
//let NET_URL_SCHEME = "comrepowernet-logintoken"

let NET_URL_SCHEME = "comrepowernet"
let NET_ITUNES_URL = URL(string: "https://itunes.apple.com/it/app/todo")
let NET_STORE_URL = URL(string: "itms://itunes.apple.com/it/app/todo")

// SpotLight
let domainIdentifier =  "com.repower.ETCA"

// Notification
let kShowAlertMessage = "ShowAlertMessage"
let kLogout = "Logout"
let birthdayIdentifier =  "\(domainIdentifier).Birthdays"

