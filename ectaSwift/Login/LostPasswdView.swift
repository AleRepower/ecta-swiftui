//
//  LostPasswdView.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 30/11/2020.
//  Copyright © 2020 Repower. All rights reserved.

import SwiftUI

struct LostPasswdView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @State var username: String = ""
    
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
            }) {
                HStack {
                    Text("Indietro") //new Back button title
                }
            }
        }
    
    var body: some View {
        NavigationView {
            VStack {
                Image("splash-square")
                Spacer()

            Text("Se hai dimenticato la password inserisci il tuo nome utente per richiedere una nuova password.")
                .foregroundColor(.gray)
                .padding(.top, 10)
                .padding(.leading, 25)
                .padding(.trailing, 25)
                .font(.system(size: 13))
           
            TextField("Inserisci Nome Utente", text: $username)
                .frame(width: 300, height: 45)
                .background(Color(.white))
//                    .background(Color(.white) .border(Color.black, width: 2))
//                    .border(Color.black, width: 2)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .autocapitalization(.none)
                .disableAutocorrection(true)
                .font(Font.system(size: 16))
                .multilineTextAlignment(.center)
                .cornerRadius(22)
            
            Button(action: {
                print("User name -> \(username)")
                if (!username.isEmpty) {
                    let params: [String:String] = ["username": username]
//                    if !api.loginApporto(params: params) {
//
//                    }
                } else {
                    api.appUtilities.showAlert("Dai un'occhiata alle credenziali, sembra ci sia qualcosa che non va.", titleMessage: "Attenzione", titleCancelButton: "OK")
        
                }
                
            }) {
                Text("INVIA")
                    .padding(13)
                    .frame(width: 80, height: 40)
                    .foregroundColor(.white)
                    .background(Color(red: 254.3/255, green: 69.7/255, blue: 59.0/255))
                    .cornerRadius(10)
                    .font(.system(size: 16))
            }
            .padding(.top, 25)
                
                Spacer()
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton)

    }
}

struct LostPasswdView_Previews: PreviewProvider {
    static var previews: some View {
        LostPasswdView()
    }
}
