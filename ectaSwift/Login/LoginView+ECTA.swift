//
//  LoginView+ECTA.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 04/11/21.
//  Copyright © 2021 REPower SpA. All rights reserved.
//

import SwiftUI


extension LoginView {
    
    func loadEctaBasicDataSets(usrDict: NSDictionary) {
        
        let jsonPayload = usrDict["json"] as? NSDictionary
        print("method --> \(usrDict["method"] as! String)")
//***********************************     LOGIN
        if ((usrDict["success"] as! Bool == true && usrDict["method"] as! String == URL_POST_LOGIN) && (jsonPayload?["idUtente"] as! Int > 0 ))  {
            isLoggedIn = true
            idUtente = jsonPayload?["idUtente"] as! Int
            username = jsonPayload?["username"] as! String
            theUsername = username
            let aziendaDefaultDict = jsonPayload?["aziendaDefault"] as? [String: Any]
            aziendaDefault = AziendaDefault(idAzienda: aziendaDefaultDict?["idAzienda"] as! Int, partitaIva: aziendaDefaultDict?["partitaIva"] as! String, ragioneSociale: aziendaDefaultDict?["ragioneSociale"] as! String)
           
            print("🔹 ECTA USER \(theUsername) - UserID: \(idUtente) IS LOGGED IN !!!! 🔹")
            print("aziendaDefaultStruct -> \(aziendaDefault) ")

//  GETTING Azienda info
            getAziendaInfo()
            // show menu
            self.isShowingMenuView = true
//************************************    LOGOUT
        }else if (usrDict["success"] as! Bool == true && usrDict["method"] as! String == URL_POST_LOGOUT )  {
            print("🔹 LOGOUT NOTIFICATION IN LOGINVIEW !!!! 🔹")
        
            isLoggedIn = false
            idUtente = 0
            username = ""
            password = ""
            theUsername = username
            self.isShowingMenuView = false
            
//************************************   INFO AZIENDA
        }else if (usrDict["success"] as! Bool == true && usrDict["method"] as! String == URL_GET_AZIENDA_INFO )  {
            
            if let prodottiArray = jsonPayload?["prodotti"] as? [[String: Any]] {
                for prodotto: [String: Any] in prodottiArray {
                    print("🔵 PRODOTTO       -> \(String(describing: prodotto))")
                }
            }
            if let sediArray = jsonPayload?["sedi"] as? [[String: Any]] {
                for sede: [String: Any] in sediArray {
                    print("🔵 SEDE       -> \(String(describing: sede))")
                }
            }
            let consulente = jsonPayload?["consulente"] as? [String: Any]
            let rappresentanteLegale = jsonPayload?["rappresentanteLegale"] as? [String: Any]
            
            
            print("🔵 CONSULENTE -> \(String(describing:consulente))")
            print("🔵 RAP LEGALE -> \(String(describing:rappresentanteLegale))")
            
            if !api.genericEctaGetDataSet(method: URL_GET_SERVIZI_EXTRA, params: ["idAzienda": String(aziendaDefault!.idAzienda)]) {
                api.appUtilities.showAlert("Call to \(URL_GET_SERVIZI_EXTRA) failed.", titleMessage: "Attenzione", titleCancelButton: "OK")
            }
                
           
//************************************   SERVIZI EXTRA
        }else if (usrDict["success"] as! Bool == true && usrDict["method"] as! String == URL_GET_SERVIZI_EXTRA )  {
            
            let moduli = jsonPayload?["moduli"] as? [[String: Any]]
            print("🔵 SERVIZI EXTRA MODULI -> \(String(describing: moduli))")
            
            if !api.genericEctaGetDataSet(method: URL_GET_DATASET_STATI_PAGAMENTO) {
                api.appUtilities.showAlert("Call to \(URL_GET_DATASET_STATI_PAGAMENTO) failed.", titleMessage: "Attenzione", titleCancelButton: "OK")
            }
                
//************************************   STATI PAGAMENTO
        }else if (usrDict["success"] as! Bool == true && usrDict["method"] as! String == URL_GET_DATASET_STATI_PAGAMENTO )  {
            
                let statiPagamento = jsonPayload?["statiPagamento"] as? [[String: Any]]
                print("🔵 STATI PAGAMENTO -> \(String(describing: statiPagamento))")
                
            
            if !api.genericEctaGetDataSet(method: URL_GET_CONTATORI, params: ["idAzienda": String(aziendaDefault!.idAzienda)]) {
                api.appUtilities.showAlert("Call to \(URL_GET_CONTATORI) failed.", titleMessage: "Attenzione", titleCancelButton: "OK")
            }
            
//************************************   CONTATORI
        }else if (usrDict["success"] as! Bool == true && usrDict["method"] as! String == URL_GET_CONTATORI )  {
            contatori = jsonPayload?["statiPagamento"] as? [Contatore] ?? [Contatore()]
            print("🔵 CONTATORI -> \(String(describing: contatori))")
//            getFatture()
            
//************************************   FATTURE
        }else if (usrDict["success"] as! Bool == true && usrDict["method"] as! String == URL_POST_FATTURE )  {
            let fatture = jsonPayload?["fatture"] as? [[String: Any]]
            print("🔵 FATTURE -> \(String(describing: fatture))")
                        
        }else{
            print("🔴 🔴 🔴 🔴 **** SOMETHING VERY BAD HAPPEN **** 🔴 🔴 🔴 🔴 ")
            print("🔴 🔴 🔴 🔴 ERROR in \(usrDict["method"] ?? "Unknow method") 🔴 🔴 🔴 🔴 ")
        }

    }
    
    
    func getAziendaInfo() {
//         https://api.ects.repower.com:8001/DataService.svc/getaziendainfo?idAzienda=70725
       
        if !api.datasetAziendaInfo(params: ["idAzienda": String(aziendaDefault!.idAzienda)]) {
            api.appUtilities.showAlert("getAziendaInfo failed.", titleMessage: "Attenzione", titleCancelButton: "OK")
        }
    }
    
    func getFatture() {
//        URL:
//        https://api.ects.repower.com:8001/DataService.svc/getfatture?
//        PARAMS:
//            codiceMeter = IT001E33745232;
//            idAzienda = 70725;
//            idTipoDocumento = 1;
//            tipoProdotto = EE;
        if !api.genericEctaPostDataSet(method: URL_POST_FATTURE, params: [ "codiceMeter" : "", "idAzienda": String(aziendaDefault!.idAzienda), "idTipoDocumento" : "1", "tipoProdotto": "EE"]) {
            api.appUtilities.showAlert("Call to \(URL_POST_FATTURE) failed.", titleMessage: "Attenzione", titleCancelButton: "OK")
        }
        
    }
    
}
