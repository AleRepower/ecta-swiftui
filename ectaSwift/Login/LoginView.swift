//
//  LoginView.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 27/11/2020.
//  Copyright © 2020 Repower. All rights reserved.

import Foundation
import SwiftUI


struct LoginView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var username: String = "alessandro.grigiante@repower.com"
    @State var password: String = "Vnner2910"
//    @State var username: String = "repower@repower.com"
//    @State var password: String = "007etca"
    @State var isShowingMenuView = false
   
    
    
    
    var body: some View {
        
        ZStack {
            NavigationView {
                VStack {
                    
                    Image("splash-square")
                    Spacer()
                    Text("Login")
                        .foregroundColor(Color(red: 89.5/255, green: 87.9/255, blue: 90.3/255))
                        .font(.custom("TheMixB-W7Bold", size: 24))
                        .fontWeight(.bold)
                        .multilineTextAlignment(.center)
                        .padding(.bottom, 25)
                    TextField("Nome Utente", text: $username)
                        .frame(width: 300, height: 45)
                        .background(Color(.white))
    //                    .background(Color(.white) .border(Color.black, width: 2))
    //                    .border(Color.black, width: 2)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .font(Font.system(size: 16))
                        .multilineTextAlignment(.center)
                        .cornerRadius(22)
                        
                    SecureField("Password", text: $password)
                        .frame(width: 300, height: 45)
                        .background(Color(.white))
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .font(Font.system(size: 16))
                        .multilineTextAlignment(.center)
                        .cornerRadius(22)

                    Button(action: {
                        self.disabled(true)
                        print("User name -> \(username) with password -> \(password)")
                        if (!username.isEmpty && !password.isEmpty) {
                            let params: [String:String] = ["username": username, "password": password]
                            if !api.loginECTA(params: params) {
                                api.appUtilities.showAlert("Il server ha rifiutato l'accesso.", titleMessage: "Attenzione", titleCancelButton: "OK")
                            }
                            
                        } else {
                            api.appUtilities.showAlert("Dai un'occhiata alle credenziali, sembra ci sia qualcosa che non va.", titleMessage: "Attenzione", titleCancelButton: "OK")
                        }
                    }) {
                        Text("ENTRA")
                            .padding(13)
                            .frame(width: 250, height: 45)
                            .foregroundColor(.white)
                            .background(Color(red: 254.3/255, green: 69.7/255, blue: 59.0/255))
                            .cornerRadius(22)
                            .font(.custom("TheMixB-W3Light", size: 18))
                    }
                    .padding(.top, 25)
                    .onReceive(NotificationCenter.default.publisher(for: Notification.Name(rawValue: kRepowerApiNotification))) { value in
                        print("🔹🔹🔹 SwiftUI NOTIFICATION IN LOGINVIEW received! WOW!! 🔹🔹🔹")
                        print("--> \(value.userInfo!)")
                        let usrDict: Dictionary = value.userInfo!
                            
                        loadEctaBasicDataSets(usrDict: usrDict as NSDictionary)
                        
                    } // onReceive end
                    
                    
                    NavigationLink(destination: LostPasswdView()) {
                          Text("Hai dimenticato la password?")
                    }.buttonStyle(PlainButtonStyle())
                    .foregroundColor(.gray)
                    .padding(.top, 10)
                    .font(.custom("TheMixB-W3Light", size: 13))
                    
                    Spacer()
                    NavigationLink(destination: MainContentView(), isActive: $isShowingMenuView) {
                    }.hidden()
            
                }
            }
        }
        .frame(maxWidth: .infinity, alignment: .topLeading)
        .edgesIgnoringSafeArea(.all)
    }
    
}


struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

