//
//  MainView.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 03/11/21.
//  Copyright © 2021 REPower SpA. All rights reserved.
//

import SwiftUI

struct MainView: View {
    var selectedItem: Menu
    @Binding var showSideMenu: Bool
    @State var areExpanded = [false, false, false, false, false, false, false, false, false, false, false, false]
    // State vars for debugging only
    @State var isExpanded = false
//    @State var ssMock = false

    
    var body: some View {
        ZStack() {
            Rectangle()
                .fill(Color.white)
                .gesture(DragGesture().onEnded({ _ in
                    resetExpanded()
                    self.showSideMenu.toggle()
                }))
                .shadow(color: Color.gray, radius: 10.0, x: 0.0, y: 0.0)
            VStack(alignment: .center) {
                Spacer()
                    .frame(height: 25.0)
//                top HEADER
                HStack {
                    Button(action: {
                        resetExpanded()
                        self.showSideMenu.toggle()
                    }, label: {
                        Image(systemName: "line.horizontal.3")
                            .imageScale(.large)
                            .foregroundColor(Color.black)
                    })
                        .padding(.leading, 20.0)
                    Spacer()
                    Text(selectedItem.title)
                    .foregroundColor(Color(selectedItem.color))
                    .font(.custom("TheMixB-W7Bold", size: 15))
                    Spacer()
                    if selectedItem.viewType != .SUP {
                        Button(action: {
                            print("Button Consulente pressed")
                        }) {
                            Image("consulente_normal_bar")
                            .padding(.trailing, 15)
                        }
                    }else{
                        Button(action: {
                            print("Button Consulente pressed")
                        }) {
                            Image("consulente_normal_bar")
                            .padding(.trailing, 15)
                        }.hidden()
                    }
                }
                
//              Box grigio  CAMBIA Azienda
                if selectedItem.viewType != .SUP {
                    HStack {
                        ZStack() {
                        Rectangle()
                            .fill(lightGray)
                            .frame( height: 50, alignment: .top)
                        
                            HStack {
                                Text(aziendaDefault?.ragioneSociale ?? "Azienda default not found!")
                                .foregroundColor(Color.black)
                                .font(.custom("TheMixB-W7Bold", size: 18))
                                
                                .fontWeight(Font.Weight.bold)
                                .padding(.leading, 15)
                            Spacer()
                            Button(action: {
                                print("Cambia Azienda")
                            }, label: {
                                Text("CAMBIA")
                                    .foregroundColor(Color.gray)
                                    .font(.custom("TheMixB-W5Plain", size: 13))
                                    .padding(.trailing, 15)
                            })
                            }
                        }
                    }
                } //end if
                
//              START BOXES
                Spacer(minLength: 30)
                ScrollView() {
                
                    let menuSpecs = getViewTypeMenu(viewType: selectedItem.viewType)
                    ForEach(menuSpecs.indices, id: \.self) { nn in

                            VStack() {
                            DisclosureGroup(menuSpecs[nn].title, isExpanded: $areExpanded[nn]) {
                                VStack{
                                   Rectangle()
                                        .fill(lightGray)
                                        .frame(height: 170)
                                        .tag(menuSpecs[nn].title)
                                        .onAppear(){
                                            
                                            if selectedItem.viewType == .EE && selectedItem.title == "FATTURAZIONE" {
                                                print("---> call fatture \(menuSpecs[nn].title)")
//                                                getFatture()
                                            }
                                        }
                                }
                            }
                            .accentColor(.white)
                            .foregroundColor(.white)
                            .font(.custom("TheMixB-W5Plain", size: 18))
                            .padding(4)
                            .background(Color(selectedItem.color))
//                            .frame(width:  UIScreen.main.bounds.size.width * 0.90, height: 45, alignment: .top)
                            
                            if !menuSpecs[nn].subTitle.isEmpty {
 
                                Button(action: {
                                    print("Pressed \(menuSpecs[nn].subTitle)")
                                }, label: {
                                    Image(systemName: "plus.circle")
                                        .foregroundColor(Color.white)
                                        .padding(5)
                                    Text(menuSpecs[nn].subTitle)
                                        .font(.custom("TheMixB-W7Bold", size: 13))
                                })
                                    .frame(width: UIScreen.main.bounds.size.width * 0.94, height: 30, alignment: .leading )
                                    .background(Color(selectedItem.color))
                                .foregroundColor(.white)
                                .offset(x: 0, y: -10)
//
                                }
                            }
                            .padding(.all)
//                            .padding(.bottom, 30)
                        }
                }
                
            } //.padding(.all) // end of VSTACK
        }// End of ZStack
        .edgesIgnoringSafeArea(.all)
        .offset(x: showSideMenu ? UIScreen.main.bounds.size.width * 0.75 : 0.0, y: 0.0)
        .animation(.spring())
        
        
    }
    
    func resetExpanded()  {
        for n in areExpanded.indices {
            areExpanded[n] = false
        }
    }
}

//struct MainView_Previews: PreviewProvider {
//    static var previews: some View {
//        MainView()
//    }
//}
