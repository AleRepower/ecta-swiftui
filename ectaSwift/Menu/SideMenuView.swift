//
//  SideMenuView.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 03/11/21.
//  Copyright © 2021 REPower SpA. All rights reserved.
//


import SwiftUI
import Combine

struct SideMenuView: View {
    
    @Binding var selectedItem: Menu
    @Binding var showSideMenu: Bool
    
    var body: some View {
                
        VStack(spacing: 0.0)  {
            Spacer(minLength: 30.0)
                Divider()
                ForEach(menus, id: \.title) { item in
                    MenuCell(showSideMenu: self.$showSideMenu, selectedItem: self.$selectedItem, item: item).background(Color.white)
                    Divider()
                }
            
                VStack (alignment: .leading, spacing: 0.0) {
                    Group {
                    Button(action: {
                       print("--> NOTIFICHE")
                    }, label: {
                        Image(systemName: "arrow.forward.circle")
                            .foregroundColor(.gray)
                        Text("NOTIFICHE")
                            .font(.custom("TheMixB-W5Plain", size: 15))
                    })
                    .background(ultraLightGray)
                    .foregroundColor(.gray)
                    .padding(.leading, 25)
                    .frame( height: 30)
                    
                    Divider()
                    
                    Button(action: {
                       print("--> MIX ENERGETICO")
                    }, label: {
                        Image(systemName: "arrow.forward.circle")
                            .foregroundColor(.gray)
                        Text("MIX ENERGETICO")
                            .font(.custom("TheMixB-W5Plain", size: 15))
                    })
                    .background(ultraLightGray)
                    .foregroundColor(.gray)
                    .padding(.leading, 25)
                    .frame( height: 30)
                    
                    Divider()
                    
                    Button(action: {
                       print("--> PRIVACY")
                    }, label: {
                        Image(systemName: "arrow.forward.circle")
                            .foregroundColor(.gray)
                        Text("PRIVACY")
                            .font(.custom("TheMixB-W5Plain", size: 15))
                    })
                    .background(ultraLightGray)
                    .foregroundColor(.gray)
                    .padding(.leading, 25)
                    .frame( height: 30)
                    
                    Divider()
                    }
                    Group {
                    Spacer(minLength: 80.0)
                    
                    Divider()
                    
                    Button(action: {
                       print("--> SEGNALA UN PROBLEMA CON L'APP")
                    }, label: {
                        Text("SEGNALA UN PROBLEMA CON L'APP")
                            .font(.custom("TheMixB-W5Plain", size: 13))
                    })
                    .background(ultraLightGray)
                    .foregroundColor(.gray)
                    .padding(.leading, 25)
                    .frame( height: 30)

                    Divider()

                    Button(action: {
                       print("--> LOGOUT")
                        
                        api.logoutECTA()
                    }, label: {
                        let logoutTxt: String = "LOGOUT \(theUsername ?? "No User Name Found!")"
                        Text(logoutTxt)
                            .font(.custom("TheMixB-W5Plain", size: 13))
                    })
                    .background(ultraLightGray)
                    .foregroundColor(.gray)
                    .padding(.leading, 25)
                    .frame( height: 30)

                    Divider()
                    }
                }
                .background(ultraLightGray)
                .foregroundColor(.gray)
//                .padding(.leading, 10)
        
                Rectangle()
                    .fill(ultraLightGray)
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                    .edgesIgnoringSafeArea(.all)
            }
            
        }
}

struct MenuCell: View {
    
    @Binding var showSideMenu:Bool
    @Binding var selectedItem: Menu
    var item:Menu
    
//    @ObservedObject var SharedSideMenu = SideMenuAction.shareInstance
     
    var body: some View {

            HStack {
                Image(item.icon)
                    .frame(width: 30.0, height: 30.0)
                Text(item.title)
                    .foregroundColor(.gray)
                    .font(.custom("TheMixB-W5Plain", size: 14))
                    .padding(.leading, 10)
//                    .multilineTextAlignment(.leading)
                Spacer()
            }
            .frame(height: 50)
            .background(Color.white)
            .padding(.leading, 20)
            .onTapGesture(perform: {
                withAnimation {
                    selectedItem = self.item
                    print("pressed CELL -> \(self.item.title)")

                    self.showSideMenu.toggle()
                }
            })
    }
}


//struct SideMenuView_Previews: PreviewProvider {
//    @State private var ssMock = false
//    @State private var selectedItem = menus[1]
//    static var previews: some View {
//
////        SideMenuView(selectedItem: $selectedItem, showSideMenu: $ssMock)
//    }
//}

