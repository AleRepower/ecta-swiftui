//
//  MenuView.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 30/11/2020.
//  Copyright © 2020 Repower. All rights reserved.

import SwiftUI

struct MenuView: View {

    @State private var isShowingMapView = false
    @State private var isShowingMap2View = false
    @State private var isShowingListView = false
    
    var body: some View {
        
        NavigationView {
            
            VStack{
                Menu("Apri Menu") {
                    Button("Cancella", action: {})
                    Menu("Altro...") {
                            Button("Rinomina", action: {})
                            Button("Developer Mode", action: {})
                    }
                    Button("Load Struttura", action: {
                        print("Loading List Struttura  --> ")
                        api.struttura()
                        //self.isShowingListView = true
                    })
                    Button("Show Struttura", action: {
                        print("Showing List Struttura  --> ")
                        
                        self.isShowingListView = true
                    })
                    Button("Mappa 2", action: {
                        print("Open MAp 2 --> ")
                        self.isShowingMap2View = true
                    })
            
                    Button("Mappa", action: {
                        print("Open MAp --> ")
                        self.isShowingMapView = true
                    })
                }
                Spacer()
            }
        }.onAppear {
            print("MenuView idUtente --> \(idUtente)")
            print("MenuView aziendaDefault --> \(aziendaDefault?.ragioneSociale)")
            print("MenuView idAzienda --> \(aziendaDefault?.idAzienda)")
        }
            
        NavigationLink(destination: MapView(), isActive: $isShowingMapView) {
        }.hidden()
        
        NavigationLink(destination: Map2View(), isActive: $isShowingMap2View) {
        }.hidden()
        
        NavigationLink(destination: ListView(), isActive: $isShowingListView) {
        }.hidden()
        
    }
}


struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
    }
}
