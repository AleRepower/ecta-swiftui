//
//  MenuSettings.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 03/11/21.
//  Copyright © 2021 REPower SpA. All rights reserved.
//

import SwiftUI

var menus = [Menu(title: "SUPPORTO", icon: "consulente", badge: 0, color: (#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)), viewType: .SUP),
             Menu(title: "ENERGIA ELETTRICA", icon: "energia_elettrica", badge: 0, color: (#colorLiteral(red: 0.6078431373, green: 0.007843137255, blue: 0.3294117647, alpha: 1)), viewType: .EE),
             Menu(title: "GAS NATURALE", icon: "gas_naturale", badge: 0, color: (#colorLiteral(red: 1, green: 0.7137254902, blue: 0.05882352941, alpha: 1)), viewType: .GAS),
             Menu(title: "ENERGIA DAL CUORE VERDE", icon: "cuore_verde", badge: 0, color: (#colorLiteral(red: 0.6352941176, green: 0.6784313725, blue: 0, alpha: 1)), viewType: .EDCV),
             Menu(title: "MOBILITA ELETTRICA", icon: "soluzioni_produttori", badge: 0, color: (#colorLiteral(red: 0, green: 0.5058823529, blue: 0.6705882353, alpha: 1)), viewType: .ME)]

let lightGray: Color = Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
let ultraLightGray: Color = Color(#colorLiteral(red: 0.9564899802, green: 0.9621830583, blue: 0.9762647748, alpha: 1))

var menuEE = [MenuSpec(title: "CONSUMI", subTitle: "VEDI DETTAGLI", hasDropDown: true),
              MenuSpec(title: "COSTI", subTitle: "VEDI DETTAGLI",hasDropDown: true),
              MenuSpec(title: "FATTURAZIONE", subTitle: "ELENCO FATTURE",hasDropDown: true),
              MenuSpec(title: "PENALI PER ENERGIA REATTIVA", subTitle: "VEDI DETTAGLI",hasDropDown: true),
              MenuSpec(title: "PUNT'AVANTI",subTitle: "VEDI DETTAGLI", hasDropDown: true),
              MenuSpec(title: "CONTRATTO DI TRASPORTO", subTitle: "",hasDropDown: false),
              MenuSpec(title: "ONERI GENERALI", subTitle: "",hasDropDown: false)]

var menuGAS = [MenuSpec(title: "AUTOLETTURA", subTitle: "GESTISCI",hasDropDown: true),
               MenuSpec(title: "CONSUMI", subTitle: "VEDI DETTAGLI",hasDropDown: true),
               MenuSpec(title: "COSTI", subTitle: "VEDI DETTAGLI",hasDropDown: true),
               MenuSpec(title: "FATTURAZIONE", subTitle: "ELENCO FATTURE",hasDropDown: true),
               MenuSpec(title: "COPERTURA ASSICURATIVA", subTitle: "",hasDropDown: false)]

var menuEDCV  = [MenuSpec(title: "CERTIFICAZIONI", subTitle: "VEDI TUTTE",hasDropDown: true),
                 MenuSpec(title: "EFFICIENZA ENERGETICA", subTitle: "VEDI DETTAGLI",hasDropDown: true),
                 MenuSpec(title: "REPORT TERMOGRAFICI", subTitle: "VEDI TUTTI",hasDropDown: true)]

var menuME    = [MenuSpec(title: "VEICOLI", subTitle: "VEDI TUTTI",hasDropDown: false),
                 MenuSpec(title: "STRUMENTI DI RICARICA", subTitle: "VEDI TUTTI",hasDropDown: true),
                 MenuSpec(title: "TELEGESTIONE STRUMENTI", subTitle: "VEDI DETTAGLI",hasDropDown: false)]


extension Binding where Value == Bool {
    public func negate() -> Binding<Bool> {
        return Binding<Bool>(get:{ !self.wrappedValue },
            set: { self.wrappedValue = !$0})
    }
}
