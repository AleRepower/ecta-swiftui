//
//  Json.swift
//
//  Created by Repower on 26/10/14.
//  Copyright (c) 2014 Repower. All rights reserved.
//

import Foundation
import CoreData

//class Json: NSManagedObject {
//    @nonobjc public class func fetchRequest() -> NSFetchRequest<Json> {
//        return NSFetchRequest<Json>(entityName: "Json")
//    }
//    
//    @NSManaged public var data: Data
//    @NSManaged public var dateLastRetrive: Date
//    @NSManaged public var url: String
//    @NSManaged public var idUserLogged: Int32
//}
/*
class Datasets: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Datasets> {
        return NSFetchRequest<Datasets>(entityName: "Datasets")
    }
    
    @NSManaged public var nome: String
    @NSManaged public var version: String
    @NSManaged public var uri: String
    @NSManaged public var estimatedSize: Int32
}

class ClassiProfittabilita: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ClassiProfittabilita> {
        return NSFetchRequest<ClassiProfittabilita>(entityName: "ClassiProfittabilita")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idClasseProfittabilita: Int32
    @NSManaged public var descrizione: String
    
}

class Fatturati: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Fatturati> {
        return NSFetchRequest<Fatturati>(entityName: "Fatturati")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idFatturato: Int32
    @NSManaged public var descrizione: String
    
}

class FormeGiuridiche: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<FormeGiuridiche> {
        return NSFetchRequest<FormeGiuridiche>(entityName: "FormeGiuridiche")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idFormaGiuridica: Int32
    @NSManaged public var descrizione: String
    @NSManaged public var sigla: String
    
}

class NumeriDipendenti: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<NumeriDipendenti> {
        return NSFetchRequest<NumeriDipendenti>(entityName: "NumeriDipendenti")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idNumeroDipendenti: Int32
    @NSManaged public var descrizione: String
    
}

class Comuni: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comuni> {
        return NSFetchRequest<Comuni>(entityName: "Comuni")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idComune: Int32
    @NSManaged public var idProvincia: Int32
    @NSManaged public var idRegione: Int32
    @NSManaged public var descrizione: String
    @NSManaged public var longitudine: Double
    @NSManaged public var latitudine: Double
    
}

class Province: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Province> {
        return NSFetchRequest<Province>(entityName: "Province")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idProvincia: Int32
    @NSManaged public var idRegione: Int32
    @NSManaged public var descrizione: String
    @NSManaged public var sigla: String
    @NSManaged public var longitudine: Double
    @NSManaged public var latitudine: Double
    
}

class Regioni: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Regioni> {
        return NSFetchRequest<Regioni>(entityName: "Regioni")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idRegione: Int32
    @NSManaged public var descrizione: String
    @NSManaged public var longitudine: Double
    @NSManaged public var latitudine: Double
}

class SettoriMerceologici: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<SettoriMerceologici> {
        return NSFetchRequest<SettoriMerceologici>(entityName: "SettoriMerceologici")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idSettoreMerceologico: Int32
    @NSManaged public var codiceAteco: String
    @NSManaged public var descrizione: String
    
}

class StatiTrattativa: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<StatiTrattativa> {
        return NSFetchRequest<StatiTrattativa>(entityName: "StatiTrattativa")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idStatoTrattativa: Int32
    @NSManaged public var codice: String
    @NSManaged public var descrizione: String
}

class CompanyImages: NSManagedObject {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<CompanyImages> {
        return NSFetchRequest<CompanyImages>(entityName: "CompanyImages")
    }
    
    @NSManaged public var dateLastRetrive: Date
    @NSManaged public var idAzienda: Int32
    @NSManaged public var idConsulente: Int32
    @NSManaged public var idImage: Int32
    @NSManaged public var imageUri: String
    @NSManaged public var image: Data
    @NSManaged public var size: String
}
*/
