//
//  StatsTarget.swift
//  Apporto
//
//  Created by AD'A on 24/06/17.
//  Copyright © 2017 Repower. All rights reserved.
//

import Foundation

class StatsTarget: NSObject {
    var totale: Int?
    var ee_totale: Int?
    var gas_totale: Int?
    var eegas_totale: Int?
    var consumi_ee: Int?
    var consumi_gas: Int?
    var tot_contatori_ee: Int?
    var tot_contatori_gas: Int?
    var saldo: Double?
    
    init (
        totale: Int?
        , ee_totale: Int?
        , gas_totale: Int?
        , eegas_totale: Int?
        , consumi_ee: Int?
        , consumi_gas: Int?
        , tot_contatori_ee: Int?
        , tot_contatori_gas: Int?
        , saldo: Double?
        ) {
        self.totale = totale
        self.ee_totale = ee_totale
        self.gas_totale = gas_totale
        self.eegas_totale = eegas_totale
        self.consumi_ee = consumi_ee
        self.consumi_gas = consumi_gas
        self.tot_contatori_ee = tot_contatori_ee
        self.tot_contatori_gas = tot_contatori_gas
        self.saldo = saldo
    }
    
    func totali () -> (numero: Int, desc: String) {
        return Stats.formatInt(totale)
    }
    
    func eeTotali () -> (numero: Int, desc: String) {
        return Stats.formatInt(ee_totale)
    }
    
    func gasTotali () -> (numero: Int, desc: String) {
        return Stats.formatInt(gas_totale)
    }
    
    func eegasTotali () -> (numero: Int, desc: String) {
        return Stats.formatInt(eegas_totale)
    }
    
    func consumoTotale() -> (value: Int?, desc: String?) {
        let consumoEE = consumi_ee != nil ? consumi_ee : 0
        let consumoGAS = consumi_gas != nil ? consumi_gas : 0
        let tot = consumoEE! + consumoGAS! * 10 // *10 = mc to kWh
        return Stats.getConsumoEE(tot)
    }
    
    func consumoEE() -> (value: Int?, desc: String?) {
        return Stats.getConsumoEE(consumi_ee)
    }
    
    func consumoGAS() -> (value: Int?, desc: String?) {
        return Stats.getConsumoGAS(consumi_gas)
    }
    
    func totContatoriEE() -> Int {
        var tot = 0
        if let eegas_totale = eegas_totale {
            tot += eegas_totale
        }
        if let ee_totale = ee_totale {
            tot += ee_totale
        }
        
        if let totale = totale, tot == 0 {
            tot = totale
        }
        return tot
    }
    
    func totContatoriGAS() -> Int {
        var tot = 0
        if let eegas_totale = eegas_totale {
            tot += eegas_totale
        }
        if let gas_totale = gas_totale {
            tot += gas_totale
        }
        
        if let totale = totale, tot == 0 {
            tot = totale
        }
        return tot
    }
    
    func consumoMedEE() -> (value: Int?, desc: String?) {
        if let consumi_ee = consumi_ee, totContatoriEE() > 0 {
            return Stats.getConsumoEE(consumi_ee / totContatoriEE())
        }
        return (nil, nil)
    }
    
    func consumoMedGAS() -> (value: Int?, desc: String?) {
        if let consumi_gas = consumi_gas, totContatoriGAS() > 0 {
            return Stats.getConsumoGAS(consumi_gas / totContatoriGAS())
        }
        return (nil, nil)
    }
    
    func consumoMedPOD() -> (value: Int?, desc: String?) {
        if let consumi_ee = consumi_ee,
            let tot_contatori_ee = tot_contatori_ee, tot_contatori_ee > 0 {
            return Stats.getConsumoEE(consumi_ee / tot_contatori_ee)
        }
        return (nil, nil)
    }
    
    func consumoMedPDR() -> (value: Int?, desc: String?) {
        if let consumi_gas = consumi_gas,
            let tot_contatori_gas = tot_contatori_gas, tot_contatori_gas > 0 {
            return Stats.getConsumoGAS(consumi_gas / tot_contatori_gas)
        }
        
        return (nil, nil)
    }
    
    func isMoroso() -> Bool? {
        guard let saldo = saldo else { return nil }
        
        let isMoroso = saldo > 0 ? true : false
        return isMoroso
    }
    
    func saldoTotale() -> (numero: Double, desc: String) {
        return Stats.formatCurrency(saldo)
    }
    
    func saldoMed() -> (numero: Double, desc: String) {
        if let saldo = saldo, let totale = totale, totale > 0 {
            let saldoMed = saldo / Double(totale)
            return Stats.formatCurrency(saldoMed)
        }
        return (0.0, "")
    }
}
