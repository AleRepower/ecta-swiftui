//
//  StatsPrenotazioni.swift
//  Net
//
//  Created by AD'A on 18/08/15.
//  Copyright © 2015 Repower. All rights reserved.
//

import Foundation

class StatsPrenotazioni: StatsTarget {
    
    var scadenza: Int?
    var standby: Int?
    var inibite: Int?
    var contatori_firme: Int?
    var contatori_firme_30gg: Int?
    var azienda_firme: Int?
    var azienda_firme_30gg: Int?
    var azienda_firme_365gg: Int?
    
    init (totale: Int?
        , ee_totale: Int?
        , gas_totale: Int?
        , eegas_totale: Int?
        , scadenza: Int?
        , standby: Int?
        , inibite: Int?
        , contatori_firme: Int?
        , contatori_firme_30gg: Int?
        , azienda_firme: Int?
        , azienda_firme_30gg: Int?
        , azienda_firme_365gg: Int?
        , consumi_ee: Int?
        , consumi_gas: Int?
        , tot_contatori_ee: Int?
        , tot_contatori_gas: Int?
        , saldo: Double?
        ) {
        self.scadenza = scadenza
        self.standby = standby
        self.inibite = inibite
        self.contatori_firme = contatori_firme
        self.contatori_firme_30gg = contatori_firme_30gg
        self.azienda_firme = azienda_firme
        self.azienda_firme_30gg = azienda_firme_30gg
        self.azienda_firme_365gg = azienda_firme_365gg
        
        super.init(totale: totale, ee_totale: ee_totale, gas_totale: gas_totale, eegas_totale: eegas_totale, consumi_ee: consumi_ee, consumi_gas: consumi_gas, tot_contatori_ee: tot_contatori_ee, tot_contatori_gas: tot_contatori_gas, saldo: saldo)
    }
    
}
