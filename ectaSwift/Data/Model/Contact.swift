//
//  Contacts.swift
//  Net
//
//  Created by Angelo D'Ariano on 15/08/14.
//  Copyright (c) 2014 Angelo D'Ariano. All rights reserved.
//




class Contact: Decodable {
    var tipo: String?
    var etichetta: String?
    var valore: String?
    
    private enum CodingKeys: String, CodingKey {
        case tipo, etichetta, valore
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.tipo = try? container.decode(String.self, forKey: .tipo)
        self.etichetta = try? container.decode(String.self, forKey: .etichetta)
        self.valore = try? container.decode(String.self, forKey: .valore)
    }
    
    init (tipo: String?
        , etichetta: String?
        , valore: String?
        ){
        self.tipo = tipo
        self.etichetta = etichetta
        self.valore = valore
    }
}

class Recapiti: Contact {
    var latitude: Double?
    var longitude: Double?
    var distance_from_POI: Int?
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }
    
    init (tipo: String?
        , etichetta: String?
        , valore: String?
        , latitude: Double? = nil
        , longitude: Double? = nil
        , distance_from_POI: Int? = nil
        ){
        self.latitude = latitude
        self.longitude = longitude
        self.distance_from_POI = distance_from_POI
        
        super.init(tipo: tipo, etichetta: etichetta, valore: valore)
    }
}

class InfoConsulente: Contact {
    
}

class InfoStruttura: Contact {
    let consulente: Consulente?
    
    private enum CodingKeys: String, CodingKey {
        case consulente
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.consulente = try? container.decode(Consulente.self, forKey: .consulente)
        try super.init(from: decoder)
    }
    
    init (tipo: String?
        , etichetta: String?
        , valore: String?
        , consulente: Consulente? = nil
        ){
        self.consulente = consulente
        super.init(tipo: tipo, etichetta: etichetta, valore: valore)
    }
}
