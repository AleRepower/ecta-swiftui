//
//  ECTAModels.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 04/11/21.
//  Copyright © 2021 REPower SpA. All rights reserved.
//

import Foundation

struct AziendaDefault: Decodable  {
    var idAzienda: Int = 0
    var partitaIva: String = ""
    var ragioneSociale: String = ""
}

struct StatoPagamento: Decodable {
    var codice: String = ""
    var id: Int = 0
    var stato: String = ""
    var statoNdc: String = ""
}

var statiPagamento = [StatoPagamento]()

struct ServizioExtra: Decodable {
    var descrizione: String = ""
    var idAzienda: String = ""
    var metodo: String = ""
    var nome: String = ""
    var tipoProdotto: String = ""
}

var serviziExtra = [ServizioExtra]()

struct Prodotto: Decodable {
    var dataFineFornitura: Date?
    var dataInizioFornitura: Date?
    var etichetta: String = ""
    var meters: Int = 0
    var tipoProdotto: String = ""
}

var prodotti = [Prodotto]()

struct Consulente: Decodable {
    var cognome: String = ""
    var email: String = ""
    var idConsulente: Int = 0
    var nome: String = ""
    var recapitoCommerciale: String = ""
}

var consulente = Consulente()

struct RappresentanteLegale: Decodable {
    var cognome: String = ""
    var nome: String = ""
    var qualifica: String = ""
}

var rappresentanteLegale = RappresentanteLegale()

struct Contatto: Decodable {
    var etichetta: String = ""
    var tipo: String = ""
    var valore: String = ""
}

var contatti = [Contatto]()

struct Location: Decodable {
    var tipo: String = ""
    var address: String = ""
    var cap: String = ""
    var idComune: String = ""
    var lat: Double = 0.0
    var lng: Double = 0.0
}

var location = Location()

struct Contatore: Decodable {
    var codiceMeter: String = ""
    var dataFineFornitura: Date?
    var dataInizioFornitura: Date?
    var distributore: Location = Location()
    var nome: String = ""
    var riferimentoTelefonico: String = ""
    var formula: String = ""
    var idMeter: String = ""
    var theLocation: Location = Location()
    var presenzaCorrettore: String = ""
    var tipoFornitura: String = ""
    var tipoProdotto: String = ""
}

var contatori = [Contatore]()

struct Fattura: Decodable {
    var amount: String = ""
    var dataFattura: Date?
    var dataRiferimento: Date?
    var docUrl: String = ""
    var idAzienda: Int = 0
    var idStatoPagamento: String = ""
    var meters: String = ""
    var numero: String = ""
    var numeroFattura: String = ""
    var pagamentoElettronico: String = ""
    var prefisso: String = ""
    var statoPagamento: String = ""
    var tipoDocumento: String = ""
    
}

var fatture = [Fattura]()
