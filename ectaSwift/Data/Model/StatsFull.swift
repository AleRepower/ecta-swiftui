//
//  StatsFull.swift
//  Net
//
//  Created by AD'A on 20/08/18.
//  Copyright © 2018 Repower. All rights reserved.
//

import Foundation

struct StatsConsulenti: Decodable, Sequence {
    let consulenti: [StatsConsulente]
    
    private enum CodingKeys: String, CodingKey {
        case consulenti
    }
    
    func makeIterator() -> AnyIterator<StatsConsulente> {
        return AnyIterator(consulenti.makeIterator())
    }
}

struct StatsConsulente: Decodable {
    let nome: String?
    let cognome: String?
    let id_consulente: Int?
    let data_ultima_firma: Date?
    let data_ultimo_evento_firma: Date?
    let prenotazioni: StatsFullPrenotazioni?
    let clienti: StatsFullClienti?
    let ex_clienti: StatsFullExClienti?
}

class StatsFullMain: Decodable {
    let aziende: Int?
    let saldo: Double?
    let nodi: [Nodo]?
    let forniture: Forniture?
    let vas: Vas?
    
    private enum CodingKeys: String, CodingKey {
        case aziende, saldo, nodi, forniture, vas
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.aziende = try? container.decode(Int.self, forKey: .aziende)
        self.saldo = try? container.decode(Double.self, forKey: .saldo)
        self.nodi = try? container.decode([Nodo].self, forKey: .nodi)
        self.forniture = try? container.decode(Forniture.self, forKey: .forniture)
        self.vas = try? container.decode(Vas.self, forKey: .vas)
    }
    
    init(aziende: Int?, saldo: Double?, nodi: [Nodo]?, forniture: Forniture?, vas: Vas?) {
        self.aziende = aziende
        self.saldo = saldo
        self.nodi = nodi
        self.forniture = forniture
        self.vas = vas
    }
    
    func isMoroso() -> Bool? {
        guard let saldo = saldo else { return nil }
        
        let isMoroso = saldo > 0 ? true : false
        return isMoroso
    }
    
    func saldoTotale() -> (numero: Double, desc: String) {
        return Stats.formatCurrency(saldo)
    }
    
    func saldoMed() -> (numero: Double, desc: String) {
        if let saldo = saldo, let aziende = aziende, aziende > 0 {
            let saldoMed = saldo / Double(aziende)
            return Stats.formatCurrency(saldoMed)
        }
        return (0.0, "")
    }
}

class StatsFullPrenotazioni: StatsFullMain {
    let limite: Int?
    let scadenza: Int?
    let standby: Int?
    let inibite: Int?
    let nodo_firme: Int?
    let nodo_firme_30gg: Int?
    let nodo_firme_365gg: Int?
    let azienda_firme: Int?
    let azienda_firme_30gg: Int?
    let azienda_firme_365gg: Int?

    
    private enum CodingKeys: String, CodingKey {
        case limite, scadenza, standby, inibite, nodo_firme, nodo_firme_30gg, nodo_firme_365gg, azienda_firme, azienda_firme_30gg, azienda_firme_365gg
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.limite = try? container.decode(Int.self, forKey: .limite)
        self.scadenza = try? container.decode(Int.self, forKey: .scadenza)
        self.standby = try? container.decode(Int.self, forKey: .standby)
        self.inibite = try? container.decode(Int.self, forKey: .inibite)
        self.nodo_firme = try? container.decode(Int.self, forKey: .nodo_firme)
        self.nodo_firme_30gg = try? container.decode(Int.self, forKey: .nodo_firme_30gg)
        self.nodo_firme_365gg = try? container.decode(Int.self, forKey: .nodo_firme_365gg)
        self.azienda_firme = try? container.decode(Int.self, forKey: .azienda_firme)
        self.azienda_firme_30gg = try? container.decode(Int.self, forKey: .azienda_firme_30gg)
        self.azienda_firme_365gg = try? container.decode(Int.self, forKey: .azienda_firme_365gg)
        try super.init(from: decoder)
    }
    
    init(aziende: Int?, nodi: [Nodo]?
        , limite: Int?,scadenza: Int?,standby: Int?,inibite: Int?,nodo_firme: Int?,nodo_firme_30gg: Int?,nodo_firme_365gg: Int?,azienda_firme: Int?,azienda_firme_30gg: Int?,azienda_firme_365gg: Int?) {
        self.limite = limite
        self.scadenza = scadenza
        self.standby = standby
        self.inibite = inibite
        self.nodo_firme = nodo_firme
        self.nodo_firme_30gg = nodo_firme_30gg
        self.nodo_firme_365gg = nodo_firme_365gg
        self.azienda_firme = azienda_firme
        self.azienda_firme_30gg = azienda_firme_30gg
        self.azienda_firme_365gg = azienda_firme_365gg
        super.init(aziende: aziende, saldo: nil, nodi: nodi, forniture: nil, vas: nil)
    }
}

class StatsFullClienti: StatsFullMain {
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }
    init(aziende: Int?, saldo: Double?, forniture: Forniture?, vas: Vas?) {
        super.init(aziende: aziende, saldo: saldo, nodi: nil, forniture: forniture, vas: vas)
    }
}

class StatsFullExClienti: StatsFullMain {
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }
    init(aziende: Int?, saldo: Double?, forniture: Forniture?, vas: Vas?) {
        super.init(aziende: aziende, saldo: saldo, nodi: nil, forniture: forniture, vas: vas)
    }
}

class Nodo: FornitureProdotto {
    let tipo: String?
    let firme: Int?
    let firme_30gg: Int?
    let firme_365gg: Int?
    let azienda_firme: Int?
    let azienda_firme_30gg: Int?
    let azienda_firme_365gg: Int?
    
    private enum CodingKeys: String, CodingKey {
        case contatori = "totale"
        case tipo, firme, firme_30gg, firme_365gg, azienda_firme, azienda_firme_30gg, azienda_firme_365gg
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.tipo = try? container.decode(String.self, forKey: .tipo)
        //self.totale = try? container.decode(Int.self, forKey: .totale)
        self.firme = try? container.decode(Int.self, forKey: .firme)
        self.firme_30gg = try? container.decode(Int.self, forKey: .firme_30gg)
        self.firme_365gg = try? container.decode(Int.self, forKey: .firme_365gg)
        self.azienda_firme = try? container.decode(Int.self, forKey: .azienda_firme)
        self.azienda_firme_30gg = try? container.decode(Int.self, forKey: .azienda_firme_30gg)
        self.azienda_firme_365gg = try? container.decode(Int.self, forKey: .azienda_firme_365gg)
        try super.init(from: decoder)
        self.contatori = try? container.decode(Int.self, forKey: .contatori)
    }
    
    init(tipo: String?, nome: String?, aziende: Int?, contatori: Int?, consumi: Int?
        , firme: Int?, firme_30gg: Int?, firme_365gg: Int?, azienda_firme: Int?, azienda_firme_30gg: Int?, azienda_firme_365gg: Int?) {
        self.tipo = tipo
        self.firme = firme
        self.firme_30gg = firme_30gg
        self.firme_365gg = firme_365gg
        self.azienda_firme = azienda_firme
        self.azienda_firme_30gg = azienda_firme_30gg
        self.azienda_firme_365gg = azienda_firme_365gg
        super.init(nome: nome, aziende: aziende, contatori: contatori, consumi: consumi)
    }
}

class Forniture: Decodable, Sequence {

    let aziende: Int?
    let prodotti: [FornitureProdotto]?
    
    private enum CodingKeys: String, CodingKey {
        case aziende, prodotti
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.aziende = try? container.decode(Int.self, forKey: .aziende)
        self.prodotti = try? container.decode([FornitureProdotto].self, forKey: .prodotti)
    }
    
    init(aziende: Int?, prodotti: [FornitureProdotto]?) {
        self.aziende = aziende
        self.prodotti = prodotti
    }
    
    func makeIterator() -> AnyIterator<FornitureProdotto> {
        guard let prodotti = self.prodotti else {
            return AnyIterator{ return nil }
        }
        return AnyIterator(prodotti.makeIterator())
    }
    
    func totali () -> (numero: Int, desc: String) {
        return Stats.formatInt(self.aziende)
    }
    
    func prodottoEE() -> FornitureProdotto? {
        guard let prodotti = prodotti else { return nil }
        let prodotto = prodotti.filter({ (prodotto: FornitureProdotto) -> Bool in
            return prodotto.nome == "energia elettrica"
        }).first
        
        return prodotto ?? nil
    }

    func prodottoGAS() -> FornitureProdotto? {
        guard let prodotti = prodotti else { return nil }
        let prodotto = prodotti.filter({ (prodotto: FornitureProdotto) -> Bool in
            return prodotto.nome == "gas naturale"
        }).first
        
        return prodotto ?? nil
    }
    
    func consumiEEGAS() -> (value: Int?, desc: String?) {
        let consumiEE = self.prodottoEE()?.consumi ?? 0
        let consumiGAS = self.prodottoGAS()?.consumi ?? 0
        return Stats.getConsumoEE(consumiEE + consumiGAS * 10)
    }
}

class Vas: Decodable, Sequence {
    let aziende: Int?
    let prodotti: [VasProdotto]?
    
    private enum CodingKeys: String, CodingKey {
        case aziende, prodotti
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.aziende = try? container.decode(Int.self, forKey: .aziende)
        self.prodotti = try? container.decode([VasProdotto].self, forKey: .prodotti)
    }
    
    init(aziende: Int?, prodotti: [VasProdotto]?) {
        self.aziende = aziende
        self.prodotti = prodotti
    }
    
    func makeIterator() -> AnyIterator<VasProdotto> {
        guard let prodotti = self.prodotti else {
            return AnyIterator{ return nil }
        }
        return AnyIterator(prodotti.makeIterator())
    }
    
    func totContrattiVas() -> (value: Int, desc: String) {
        let contrattiVas = self.prodotti?.reduce(0, { (result, prodotto: VasProdotto) in return result + (prodotto.contratti ?? 0) }) ?? 0
        return Stats.formatInt(contrattiVas)
    }
}

class Prodotto: Decodable {
    let nome: String?
    let aziende: Int?
    
    private enum CodingKeys: String, CodingKey {
        case nome
        case aziende
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.nome = try? container.decode(String.self, forKey: .nome)
        self.aziende = try? container.decode(Int.self, forKey: .aziende)
    }
    
    init(nome: String?, aziende: Int?) {
        self.nome = nome
        self.aziende = aziende
    }
}

class FornitureProdotto: Prodotto {
    var contatori: Int?
    let consumi: Int?
    
    private enum CodingKeys: String, CodingKey {
        case contatori
        case consumi
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.contatori = try? container.decode(Int.self, forKey: .contatori)
        self.consumi = try? container.decode(Int.self, forKey: .consumi)
        try super.init(from: decoder)
    }
    
    init(nome: String?, aziende: Int?, contatori: Int?, consumi: Int?) {
        self.contatori = contatori
        self.consumi = consumi
        super.init(nome: nome, aziende: aziende)
    }
    
    func eeTotali () -> (numero: Int, desc: String) {
        guard let nome = nome, nome == "energia elettrica" else {
            return (0, "")
        }
        return Stats.formatInt(aziende)
    }

    func gasTotali () -> (numero: Int, desc: String) {
        guard let nome = nome, nome == "gas naturale" else {
            return (0, "")
        }
        return Stats.formatInt(aziende)
    }
    
//    func eegasTotali () -> (numero: Int, desc: String) {
//        return (99999,"non serve più")
//    }
    
    // spostato!
//    func consumoTotale() -> (consumo: Int?, desc: String?) {
//        let consumoEE = consumi_ee != nil ? consumi_ee : 0
//        let consumoGAS = consumi_gas != nil ? consumi_gas : 0
//        let tot = consumoEE! + consumoGAS! * 10 // *10 = mc to kWh
//        return Stats.getConsumoEE(tot)
//    }
    
    func consumoEE() -> (value: Int?, desc: String?) {
        guard let nome = nome, nome == "energia elettrica" else {
            return (nil, "")
        }
        return Stats.getConsumoEE(consumi)
    }

    func consumoGAS() -> (value: Int?, desc: String?) {
        guard let nome = nome, nome == "gas naturale" else {
            return (nil, "")
        }
        return Stats.getConsumoGAS(consumi)
    }
    
    func totContatoriEE() -> Int {
        guard let contatori = contatori, let nome = nome, nome == "energia elettrica" else {
            return (0)
        }
        return contatori
    }
    
    func totContatoriGAS() -> Int {
        guard let contatori = contatori, let nome = nome, nome == "gas naturale" else {
            return (0)
        }
        return contatori
    }
    
    func consumoMedEE() -> (value: Int?, desc: String?) {
        guard let consumi = consumi, let aziende = aziende, let nome = nome, nome == "energia elettrica", aziende > 0 else {
            return (nil, nil)
        }
        return Stats.getConsumoEE(consumi / aziende)
    }
    
    func consumoMedGAS() -> (value: Int?, desc: String?) {
        guard let consumi = consumi, let aziende = aziende, let nome = nome, nome == "gas naturale", aziende > 0 else {
            return (nil, nil)
        }
        return Stats.getConsumoGAS(consumi / aziende)
    }
    
    func consumoMedPOD() -> (value: Int?, desc: String?) {
        if let consumiEE = consumoEE().value, totContatoriEE() > 0 {
            return Stats.getConsumoEE(consumiEE / totContatoriEE())
        }
        return (nil, nil)
    }
    
    func consumoMedPDR() -> (value: Int?, desc: String?) {
        if let consumiGAS = consumoGAS().value, totContatoriGAS() > 0 {
            return Stats.getConsumoGAS(consumiGAS / totContatoriGAS())
        }
        return (nil, nil)
    }
    
}

class VasProdotto: Prodotto {
    let contratti: Int?
    
    private enum CodingKeys: String, CodingKey {
        case contratti
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.contratti = try? container.decode(Int.self, forKey: .contratti)
        try super.init(from: decoder)
    }
    
    init(nome: String?, aziende: Int?, contratti: Int?) {
        self.contratti = contratti
        super.init(nome: nome, aziende: aziende)
    }
}


