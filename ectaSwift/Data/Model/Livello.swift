//
//  Livello.swift
//  Net
//
//  Created by AD'A on 06/05/15.
//  Copyright (c) 2015 Repower. All rights reserved.
//

import Foundation

//class Livello {
//    var id_livello: Int?
//    var livello: String?
//    var descrizione: String?
//    var responsabile: [Responsabile]
//
//    init (id_livello: Int?
//        , livello: String?
//        , descrizione: String?
//        , responsabile: [Responsabile]
//        ){
//            self.id_livello = id_livello
//            self.livello = livello
//            self.descrizione = descrizione
//            self.responsabile = responsabile
//    }
//}
//
//class Responsabile {
//    var id_consulente: Int?
//    var cognome: String?
//    var nome: String?
//    var sesso: String?
//    var email: String?
//    var tel_commerciale: String?
//
//    init (id_consulente: Int?
//        , cognome: String?
//        , nome: String?
//        , sesso: String?
//        , email: String?
//        , tel_commerciale: String?
//        ){
//        self.id_consulente = id_consulente
//        self.cognome = cognome
//        self.nome = nome
//        self.sesso = sesso
//        self.email = email
//        self.tel_commerciale = tel_commerciale
//    }
//}

class Livello: Decodable  {
    var id_livello: Int?
    var livello: String?
    var descrizione: String?
    var responsabile: Responsabile
    
    private enum CodingKeys: String, CodingKey {
        case id_livello, livello, descrizione, responsabile
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id_livello = try? container.decode(Int.self, forKey: .id_livello)
        self.livello = try? container.decode(String.self, forKey: .livello)
        self.descrizione = try? container.decode(String.self, forKey: .descrizione)
        self.responsabile = try container.decode(Responsabile.self, forKey: .responsabile)
    }
    
//    func makeIterator() -> AnyIterator<Responsabile> {
//        return AnyIterator(responsabili.makeIterator())
//    }
    
    init (id_livello: Int?
        , livello: String?
        , descrizione: String?
        , responsabile: Responsabile
        ){
        self.id_livello = id_livello
        self.livello = livello
        self.descrizione = descrizione
        self.responsabile = responsabile
    }
}

class Responsabile: Decodable {
    var id_consulente: Int?
    var cognome: String?
    var nome: String?
    var sesso: String?
    var email: String?
    var tel_commerciale: String?
    
    private enum CodingKeys: String, CodingKey {
        case id_consulente, cognome, nome, sesso, email, tel_commerciale
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id_consulente = try? container.decode(Int.self, forKey: .id_consulente)
        self.cognome = try? container.decode(String.self, forKey: .cognome)
        self.nome = try? container.decode(String.self, forKey: .nome)
        self.sesso = try? container.decode(String.self, forKey: .sesso)
        self.email = try? container.decode(String.self, forKey: .email)
        self.tel_commerciale = try? container.decode(String.self, forKey: .tel_commerciale)
    }
    
    init (id_consulente: Int?
        , cognome: String?
        , nome: String?
        , sesso: String?
        , email: String?
        , tel_commerciale: String?
        ){
        self.id_consulente = id_consulente
        self.cognome = cognome
        self.nome = nome
        self.sesso = sesso
        self.email = email
        self.tel_commerciale = tel_commerciale
    }
}
