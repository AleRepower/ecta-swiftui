//
//  MenuItem.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 03/11/21.
//  Copyright © 2021 REPower SpA. All rights reserved.
//

import SwiftUI

public enum ViewType: String {
    case SUP
    case EE
    case GAS
    case EDCV
    case ME
}

struct Menu {
    var title:String
    var icon:String
    var badge:Int
    var color:UIColor
    var viewType:ViewType
}

struct MenuSpec {
    var title:String
    var subTitle:String
    var hasDropDown:Bool
}

func getViewTypeMenu(viewType: ViewType) -> [MenuSpec] {
    
    switch viewType {
    case .SUP:
        return []
        
    case .EE:
        return menuEE
        
    case .GAS:
        return menuGAS
        
    case .EDCV:
        return menuEDCV
    
    case .ME:
        return menuME
    }
    
}

