//
//  Stats.swift
//  Net
//
//  Created by AD'A on 12/07/15.
//  Copyright (c) 2015 Repower. All rights reserved.
//

import Foundation

class Stats: Contact {
    
    class func getConsumoEE(_ consumoEE:Int?) -> (value: Int?, desc: String?) {
        guard let consumoValue = consumoEE, consumoValue > 0 else { return (nil, nil) }
        //if consumoValue == 0 { return (nil, nil) }
        
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "it_IT")
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.maximumFractionDigits = 1

        var desc = "\(numberFormatter.string(from: NSNumber(value: consumoValue))!) kWh"
        
        if consumoValue / 1000000000 > 0 {
            let number: Float = Float(consumoValue) / 1000000000
            desc = "~\(numberFormatter.string(from: NSNumber(value: number))!) TWh"
        }
        else if consumoValue / 1000000 > 0 {
            let number: Float = Float(consumoValue) / 1000000
            desc = "~\(numberFormatter.string(from: NSNumber(value: number))!) GWh"
        }
        
        return (consumoValue, desc)
    }
    
    class func getConsumoGAS(_ consumoGAS:Int?) -> (value: Int?, desc: String?) {
        guard let consumoValue = consumoGAS, consumoValue > 0 else { return (nil, nil) }
        //if consumoValue == 0 { return (nil, nil) }
        
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "it_IT")
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.maximumFractionDigits = 1
        
        var desc = "\(numberFormatter.string(from: NSNumber(value: consumoValue))!) Smc"
        
        if consumoValue / 1000000000 > 0 {
            let number: Float = Float(consumoValue) / 1000000000
            desc = "~\(numberFormatter.string(from: NSNumber(value: number))!) mrd Smc"
        }
        else if consumoValue / 1000000 > 0 {
            let number: Float = Float(consumoValue) / 1000000
            desc = "~\(numberFormatter.string(from: NSNumber(value: number))!) mio Smc"
        }
        else if consumoValue / 1000 > 0 {
            let number: Float = Float(consumoValue) / 1000
            desc = "~\(numberFormatter.string(from: NSNumber(value: number))!) mila Smc"
        }
        
        return (consumoValue, desc)
    }
    
    
    class func formatInt(_ value: Int?) -> (Int, String) {
        let number = value == nil ? 0 : value
        
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "it_IT")
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        numberFormatter.maximumFractionDigits = 0
        
        return (number!, "\(numberFormatter.string(from: NSNumber(value: number!))!)")
    }
    
    class func formatFloat(_ value: Float?) -> (Float, String) {
        let number = value == nil ? 0.0 : value
       
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "it_IT")
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
//        numberFormatter.maximumFractionDigits = 0
        
        return (number!, "\(numberFormatter.string(from: NSNumber(value: number!))!)")
    }
    
    class func formatCurrency(_ value: Float?) -> (Float, String) {
        let number = value == nil ? 0.0 : value
        
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "it_IT")
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        //        numberFormatter.maximumFractionDigits = 0
        
        return (number!, "\(numberFormatter.string(from: NSNumber(value: number!))!)")
    }
    
    class func formatCurrency(_ value: Double?) -> (Double, String) {
        let number = value == nil ? 0.0 : value
        
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = Locale(identifier: "it_IT")
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        //        numberFormatter.maximumFractionDigits = 0
        
        return (number!, "\(numberFormatter.string(from: NSNumber(value: number!))!)")
    }
}

enum TipoStats {
    case prenotati, clienti, exclienti
    
    static func tipoStats(da tipo:String) -> TipoStats? {
        switch tipo.lowercased() {
        case "prenotati", "prenotazioni", "prenotato", "prenotazione":
            return .prenotati
        case "clienti", "cliente":
            return .clienti
        case "ex clienti", "ex cliente", "ex-clienti", "ex-cliente":
            return .exclienti
        default:
            return nil
        }
    }
    
    static func stats(from tipoStats:TipoStats, consulente:Consulente) -> (main: StatsFullMain?, ee:FornitureProdotto?, gas:FornitureProdotto?) {
        var ee: FornitureProdotto?
        var gas: FornitureProdotto?
        
        switch tipoStats {
        case .prenotati:
            guard let nodi = consulente.statsFullPrenotazioni?.nodi else { return (consulente.statsFullPrenotazioni, nil, nil)}
            for nodo in nodi {
                switch nodo.nome {
                case "energia elettrica":
                    ee = nodo
                case "gas naturale":
                    gas = nodo
                default: break
                }
            }
            return (consulente.statsFullPrenotazioni, ee, gas)
        case .clienti:
            guard let prodotti = consulente.statsFullClienti?.forniture?.prodotti else { return (consulente.statsFullClienti, nil, nil)}
            for prodotto in prodotti {
                switch prodotto.nome {
                case "energia elettrica":
                    ee = prodotto
                case "gas naturale":
                    gas = prodotto
                default: break
                }
            }
            return (consulente.statsFullClienti, ee, gas)
        case .exclienti:
            guard let prodotti = consulente.statsFullExClienti?.forniture?.prodotti else { return (consulente.statsFullExClienti, nil, nil)}
            for prodotto in prodotti {
                switch prodotto.nome {
                case "energia elettrica":
                    ee = prodotto
                case "gas naturale":
                    gas = prodotto
                default: break
                }
            }
            return (consulente.statsFullExClienti, ee, gas)
        }
    }
}
