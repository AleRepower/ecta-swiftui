//
//  Consulenti.swift
//  Net
//
//  Created by Angelo D'Ariano on 03/08/14.
//  Copyright (c) 2014 Angelo D'Ariano. All rights reserved.
//

import UIKit


struct Consulenti: Decodable, Sequence {
    let consulenti: [Consulente]
    
    private enum CodingKeys: String, CodingKey {
        case consulenti
    }
    
    func makeIterator() -> AnyIterator<Consulente> {
        return AnyIterator(consulenti.makeIterator())
    }
}

class Consulente: Decodable, Identifiable {
    
    var id_consulente: Int
    @objc var cognome: String
    var nome: String
    var data_formazione: String?
    var data_inizio_attivita: String?
    var data_ultima_firma: String?
    var data_nascita: String?
    var limite_prenotazioni: Int?
    var pic_hash: String?
    var prenotazioni: Int?
    var clienti: Int?
    var qualifica: String?
    var sesso: String?
    var ultimo_cambio_password: String?
    var ultimo_login: String?
    var recapiti: [Recapiti]
    var livelli: [Livello]
    
    // da consulente/stats
    var data_ultimo_evento_firma: Date?
    var statsPrenotazioni: StatsPrenotazioni?
    var statsClienti: StatsClienti?
    var statsExClienti: StatsExClienti?
    
    // da consulente/stats_full
    var statsFullPrenotazioni: StatsFullPrenotazioni?
    var statsFullClienti: StatsFullClienti?
    var statsFullExClienti: StatsFullExClienti?
    
    private enum CodingKeys: String, CodingKey {
        case recapiti = "contacts", livelli = "gerarchia"
        case id_consulente, cognome, nome, data_formazione, data_inizio_attivita, data_ultima_firma, data_nascita, limite_prenotazioni, pic_hash, prenotazioni, clienti, qualifica, sesso, ultimo_cambio_password, ultimo_login
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id_consulente = try container.decode(Int.self, forKey: .id_consulente)
        self.id_consulente = try container.decode(Int.self, forKey: .id_consulente)
        self.cognome = try container.decode(String.self, forKey: .cognome)
        self.nome = try container.decode(String.self, forKey: .nome)
        self.data_formazione = try? container.decode(String.self, forKey: .data_formazione)
        self.data_inizio_attivita = try? container.decode(String.self, forKey: .data_inizio_attivita)
        self.data_ultima_firma = try? container.decode(String.self, forKey: .data_ultima_firma)
        self.data_nascita = try? container.decode(String.self, forKey: .data_nascita)
        self.limite_prenotazioni = try? container.decode(Int.self, forKey: .limite_prenotazioni)
        self.pic_hash = try? container.decode(String.self, forKey: .pic_hash)
        self.prenotazioni = try? container.decode(Int.self, forKey: .prenotazioni)
        self.clienti = try? container.decode(Int.self, forKey: .clienti)
        self.qualifica = try? container.decode(String.self, forKey: .qualifica)
        self.sesso = try? container.decode(String.self, forKey: .sesso)
        self.ultimo_cambio_password = try? container.decode(String.self, forKey: .ultimo_cambio_password)
        self.ultimo_login = try? container.decode(String.self, forKey: .ultimo_login)
        self.recapiti = try container.decode([Recapiti].self, forKey: .recapiti)
        self.livelli = try container.decode([Livello].self, forKey: .livelli)
    }
    
    init (
        cognome: String
        , data_formazione: String
        , data_inizio_attivita: String
        , data_ultima_firma: String
        , data_nascita: String
        , id_consulente: Int
        , limite_prenotazioni: Int
        , nome: String
        , pic_hash: String
        , prenotazioni: Int
        , clienti: Int
        , qualifica: String
        , sesso: String
        , ultimo_cambio_password: String
        , ultimo_login: String
        , contacts: [Recapiti]
        , livelli: [Livello]
        ){
        self.cognome = cognome
        self.data_formazione = data_formazione
        self.data_inizio_attivita = data_inizio_attivita
        self.data_ultima_firma = data_ultima_firma
        self.data_nascita = data_nascita
        self.id_consulente = id_consulente
        self.limite_prenotazioni = limite_prenotazioni
        self.nome = nome
        self.pic_hash = pic_hash
        self.prenotazioni = prenotazioni
        self.clienti = clienti
        self.qualifica = qualifica
        self.sesso = sesso
        self.ultimo_cambio_password = ultimo_cambio_password
        self.ultimo_login = ultimo_login
        self.recapiti = contacts
        self.livelli = livelli
    }
    
    func valueStringForKey(_ key: String) -> (String) {
        var consulenteProperty: String = ""
        let newKey = key.lowercased()
        var managers = ""
        for manager in livelli{
            if let nome_manager = manager.responsabile.nome, let cognome_manager = manager.responsabile.cognome {
                managers += "\(nome_manager)\(cognome_manager)"
            }
        }
        switch newKey{
        case "cognome":
            consulenteProperty = cognome
        case "nome":
            consulenteProperty = nome
        case "manager":
            consulenteProperty = managers
        case "porto":
            consulenteProperty = ultimoAccessoPortoDa().desc
        case "tutti":
            consulenteProperty = "\(cognome)\(nome)\(managers)\(ultimoAccessoPortoDa().desc)"
        default:
            consulenteProperty = ""
        }
        return consulenteProperty
    }
    
    func totClienti() -> (value: Int, desc: String) {
        return Stats.formatInt(self.clienti)
    }
    
    func getDate(from date: String) -> Date? {
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH.mm.ss.S"
        return dateFormatter.date(from: date)
    }
    
    func dataNascita() -> (Date?) {
        guard let data = data_nascita else { return nil }
        return getDate(from: data)
    }
    
    func dataInizioAttivita() -> (Date?) {
        guard let data = data_inizio_attivita else { return nil }
        return getDate(from: data)
    }
    
    func dataUltimaFirma() -> (Date?) {
        guard let data = data_ultima_firma else { return nil }
        return getDate(from: data)
    }
    
    func dataFormazione() -> (Date?) {
        guard let data = data_formazione else { return nil }
        return getDate(from: data)
    }
    
    func ultimoLogin() -> (Date?) {
        guard let data = ultimo_login else { return nil }
        return getDate(from: data)
    }
    
    func natoDa() -> (data: String, anni: Int, desc: String) {
        guard let dataNascita = dataNascita() else { return ("", 0, "") }
        return Consulente.formatDateToYears(dataNascita)
    }
    
    func inAziendaDa() -> (data: String, anni: Int, mesi: Int, giorni: Int, ore: Int, desc: String) {
        return Consulente.parseDate(self.dataInizioAttivita(), isSuffixFa: false)
    }
    
    func formatoDa() -> (data: String, anni: Int, mesi: Int, giorni: Int, desc: String) {
        let parsed = Consulente.parseDate(self.dataFormazione())
        return (parsed.data, parsed.anni, parsed.mesi, parsed.giorni, parsed.desc)
    }
    
    func ultimoAccessoPortoDa() -> (orario: String, data: String, anni: Int, mesi: Int, giorni: Int, ore: Int, desc: String) {
        guard let date = self.ultimoLogin() else { return ("", "", 0, 0, 0, 0, "") }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let orario = dateFormatter.string(from: date)
        
        let parsedData = Consulente.parseDate(self.ultimoLogin())
        return (orario, parsedData.data, parsedData.anni, parsedData.mesi, parsedData.giorni, parsedData.ore, parsedData.desc)
    }

    //    func ultimaPropostaFirmataDa() -> (data: String, anni: Int, mesi: Int, giorni: Int, ore: Int, desc: String) {
    //        return Consulente.parseDate(self.dataUltimaFirma())
    //    }
    
    func ultimaPropostaFirmataDa() -> (orario: String, data: String, anni: Int, mesi: Int, giorni: Int, ore: Int, desc: String) {
        guard let date = self.dataUltimaFirma() else { return ("", "", 0, 0, 0, 0, "") }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let orario = dateFormatter.string(from: date)
        
        let parsedData = Consulente.parseDate(self.dataUltimaFirma())
        return (orario, parsedData.data, parsedData.anni, parsedData.mesi, parsedData.giorni, parsedData.ore, parsedData.desc)
    }
    
    var numberFormatter = NumberFormatter()
    
    /// Qualifiche
    enum StrutturaQualifiche {
        case struttura
        case rm
        case am
        case ra
        case con
        case sub
        case re
    }
    
    let strutturaQualificheConsulente: Dictionary<String, StrutturaQualifiche> = ["struttura":.struttura, "RM":.rm, "AM":.am, "RA":.ra,"CON":.con,"SUB":.sub,"RE":.re]
    
    /*
    func qualificaAttr() -> (imageIOS7: UIImage, colore: UIColor, desc: String) {
        let defaultColor = UIColor.repowerStrutturaDefault
        
        let qualificaAgente = qualifica ?? ""
        if qualificaAgente.isEmpty { return (UIImage(named: "struttura_face_6")!, defaultColor, "") }
        
        let qualificaConsulente: StrutturaQualifiche = strutturaQualificheConsulente[qualificaAgente]!
        
        switch qualificaConsulente {
        case .struttura:
            return (UIImage(named: "struttura_face_6")!
                , UIColor.repowerStruttura
                , "struttura")
        case .rm:
            return (UIImage(named: "struttura_face_3")!
                , UIColor.repowerStrutturaRM
                , "manager di struttura")
        case .am:
            return (UIImage(named: "struttura_face_2")!
                , UIColor.repowerStrutturaAM
                , "area manager")
        case .ra:
            return (UIImage(named: "struttura_face_1")!
                , UIColor.repowerStrutturaRA
                , "manager agenzia")
        case .con:
            return (UIImage(named: "struttura_face_4")!
                , UIColor.repowerStrutturaCON
                , "consulente")
        case .sub:
            return (UIImage(named: "struttura_face_5")!
                , UIColor.repowerStrutturaSUB
                , "subagente")
        case .re:
            return (UIImage(named: "struttura_face_6")!, defaultColor, "sede Repower")
            //        default:
            //            return (UIImage(named: "struttura_face_6")!, defaultColor, "")
        }
    }
    */
    var dateFormatter = DateFormatter()
}

extension Consulente {
    class func parseDate(_ date: Date?, isSuffixFa: Bool = true) -> (data: String, anni: Int, mesi: Int, giorni: Int, ore: Int, desc: String) {
        guard let date = date else { return ("", 0, 0, 0, 0, "") }
        let dateDa = Calendar.current.dateComponents([.year, .month, .day, .hour], from: date, to: Date())
        
        let daAnni =  dateDa.year, daMesi =  dateDa.month, daGiorni =  dateDa.day, daOre = dateDa.hour
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let data = dateFormatter.string(from: date)
        
        var dataDaStr = "", dataDaMesiStr = "", dataDaGiorniStr = ""
        
        if let daOre = daOre{
            if daOre < 1 {
                dataDaStr = "meno di 1 ora"
            } else if daOre == 1 {
                dataDaStr = "\(daOre) ora"
            } else if daOre > 1 {
                dataDaStr = "\(daOre) ore"
            }
        }
        
        if let daGiorni = daGiorni, daGiorni > 0 {
            dataDaGiorniStr = daGiorni == 1 ? "ieri" : "\(daGiorni) giorni"
            dataDaStr = dataDaGiorniStr
        }
        
        if let daMesi = daMesi, daMesi > 0 {
            dataDaMesiStr = daMesi == 1 ? "\(daMesi) mese" : "\(daMesi) mesi"
            dataDaStr = dataDaMesiStr
            if !dataDaGiorniStr.isEmpty {
                //                ultimaFirmaDaStr = "\(dataDaMesiStr) e \(dataDaGiorniStr)"
                dataDaStr = daGiorni! > 20 ? "quasi \(daMesi+1) mesi" : "poco più di \(dataDaMesiStr)"
            }
        }
        
        if let daAnni = daAnni, daAnni > 0 {
            dataDaStr = daAnni == 1 ? "\(daAnni) anno" : "\(daAnni) anni"
            if !dataDaMesiStr.isEmpty {
                dataDaStr = "\(dataDaStr) e \(dataDaMesiStr)"
            }
        }
        
        if isSuffixFa && dataDaStr != "ieri" {
            dataDaStr += " fa"
        }
        
        return (data, daAnni!, daMesi!, daGiorni!, daOre!, dataDaStr)
    }
    
    class func formatDateToYears(_ data: Date) -> (data: String, anni: Int, desc: String) {
        let dateFrom = Calendar.current.dateComponents([Calendar.Component.year], from: data, to: Date())
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let data = dateFormatter.string(from: data)
        
        let daAnni =  dateFrom.year
        var dateFromStr = ""
        
        if let daAnni = daAnni, daAnni > 0 {
            dateFromStr = daAnni == 1 ? "\(daAnni) anno" : "\(daAnni) anni"
        }
        
        return (data, daAnni!, dateFromStr)
    }
}

