//
//  Persistence.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 27/11/2020.
//  Copyright © 2020 Repower. All rights reserved.

import CoreData

struct PersistenceController {
    static let shared = PersistenceController()

    static var preview: PersistenceController = {
        let result = PersistenceController(inMemory: true)
        let viewContext = result.container.viewContext
        for _ in 0..<10 {
            let newItem = Json(context: viewContext)
            newItem.timestamp = Date()
        }
        do {
            try viewContext.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return result
    }()

    let container: NSPersistentContainer

    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "ectaSwift")
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                Typical reasons for an error here include:
                * The parent directory does not exist, cannot be created, or disallows writing.
                * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                * The device is out of space.
                * The store could not be migrated to the current model version.
                Check the error message to determine what the actual problem was.
                */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
    
    func addJson(idUserLogged: Int32, data: Data, url: String, dataAggiornamento: Date = Date()) {
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Json", in: PersistenceController.shared.container.viewContext) else {
            print(#function, "errore nella creazione dell'entity")
            return
        }
        let newJson = Json(entity: entity, insertInto: PersistenceController.shared.container.viewContext)
        newJson.idUserLogged = Int32(idUserLogged)
        newJson.data = data
        newJson.url = url
        newJson.dateLastRetrive = dataAggiornamento
        
        do {
            try PersistenceController.shared.container.viewContext.save() // la funzione save() rende persistente il nuovo oggetto in memoria
        } catch let error {
            print("""
                Errore salvataggio Json \(newJson.url) in memoria:
                \(error)
                """)
        }
    }
    
    func deleteCachedData(_ entityName: String, onlyForIdUserLogged: Bool = true) -> Bool {
        let fReq:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        
        if onlyForIdUserLogged {
            let fPred = NSPredicate(format: "(idUserLogged = %d)", idUserLogged)
            fReq.predicate = fPred
        }
        do {
            if let fResults = try PersistenceController.shared.container.viewContext.fetch(fReq) as? [NSManagedObject] {
                print("DELETE cache of '\(entityName)'")
                for objToDelete: NSManagedObject in fResults {
                    PersistenceController.shared.container.viewContext.delete(objToDelete)
                }
                do {
                    try PersistenceController.shared.container.viewContext.save() // save() rende persistente la cancellazione
                    return true
                } catch let error {
                    print("Errore cancellazione \(entityName): \(error)")
                    return false
                }
            }
        }
        catch {
            print("deleteCachedData Error: \(error)")
            return false
        }
        return false
    }
    
    func getLastCachedJson(urlPathComplete url: String)  -> [Json]? {
        var jsons: [Json]? = []
        let fReq:NSFetchRequest<NSFetchRequestResult> = Json.fetchRequest()
        
        let fPred1 = NSPredicate(format: "(url = %@)", url)
        //        print("idUserLogged = \(idUserLogged) isLoggedIn = \(isLoggedIn)")
        let fPred2 = NSPredicate(format: "(idUserLogged = %d)", idUserLogged)
        
        // Combine the two predicates above in to one compound predicate
        let fPred = NSCompoundPredicate(type: .and, subpredicates: [fPred1, fPred2])
        fReq.predicate = fPred
        
        let fSorter: NSSortDescriptor = NSSortDescriptor(key: "dateLastRetrive", ascending: false)
        fReq.sortDescriptors = [fSorter]

        
        do {
            jsons = try PersistenceController.shared.container.viewContext.fetch(fReq) as? [Json]
            return jsons
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return nil
    }
}
