//
//  Datasets.swift
//
//  Created by Repower on 27/05/17.
//  Copyright © 2017 Repower. All rights reserved.
//

import Foundation
import CoreData

enum Dataset {
    case classiProfittabilita, comuni, fatturati, formeGiuridiche, numeriDipendenti, province, regioni, settoriMerceologici, statiTrattativa
    
    static func from(nome: String) -> Dataset? {
        switch nome {
        case "classi_profittabilita":
            return .classiProfittabilita
        case "comuni":
            return .comuni
        case "fatturato":
            return .fatturati
        case "fatturati":
            return .fatturati
        case "forme_giuridiche":
            return .formeGiuridiche
        case "numero_dipendenti":
            return .numeriDipendenti
        case "numeri_dipendenti":
            return .numeriDipendenti
        case "province":
            return .province
        case "regioni":
            return .regioni
        case "settori_merceologici":
            return .settoriMerceologici
        case "stati_trattativa":
            return .statiTrattativa
        default:
            return nil
        }
    }
    
    static func coreDataEntityName(of dataset: Dataset?) -> String {
        guard let dataset = dataset else { return "" }
        
        switch dataset {
            // accertarsi che esista il corrispondente Dataset in CoreData prima di scommentare
        case .classiProfittabilita:
            return "ClassiProfittabilita"
        case .comuni:
            return "Comuni"
        case .fatturati:
            return "Fatturati"
        case .formeGiuridiche:
            return "FormeGiuridiche"
        case .numeriDipendenti:
            return "NumeriDipendenti"
        case .province:
            return "Province"
        case .regioni:
            return "Regioni"
        case .settoriMerceologici:
            return "SettoriMerceologici"
        case .statiTrattativa:
            return "StatiTrattativa"
        }
    }
}


class CurrentDatasets: NSObject {
    
    var nome: String?
    var version: String?
    var uri: String?
    var estimated_size: Int?
    
    init (nome: String?
        , version: String?
        , uri: String?
        , estimated_size: Int?
        ){
        self.nome = nome
        self.version = version
        self.uri = uri
        self.estimated_size = estimated_size
    }
    
    func isToUpdate() -> Bool {
        guard let nome =  self.nome else { return true }
        
        let entityName = "Datasets" //Dataset.coreDataEntityName(of: dataset)
        
        let fReq:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        let fPred1 = NSPredicate(format: "(nome = %@)", nome)
        //        //            //        print("idUserLogged = \(idUserLogged) isLoggedIn = \(isLoggedIn)")
        //        //            let fPred2 = NSPredicate(format: "(idUserLogged = %d)", idUserLogged)
        //        //
        //        //            // Combine the two predicates above in to one compound predicate
        //        //            let fPred = NSCompoundPredicate(type: .and, subpredicates: [fPred1, fPred2])
        fReq.predicate = fPred1
        
//        let fSorter: NSSortDescriptor = NSSortDescriptor(key: orderedBy, ascending: ascending)
//        fReq.sortDescriptors = [fSorter]
        
        let managedObjectContext = api.managedObjectContext
        var savedDatasets: [Datasets]? = nil
        do {
            savedDatasets = try managedObjectContext.fetch(fReq) as? [Datasets]
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        guard let savedDataset = savedDatasets?.first else { return true }
        return savedDataset.version != self.version
    }
    
    func deleteFromCoreData() {
        guard let nome =  self.nome else { return }
        
        let entityName = "Datasets" //Dataset.coreDataEntityName(of: dataset)
        
        let fReq:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        let fPred1 = NSPredicate(format: "(nome = %@)", nome)
        fReq.predicate = fPred1
        
        //        let fSorter: NSSortDescriptor = NSSortDescriptor(key: orderedBy, ascending: ascending)
        //        fReq.sortDescriptors = [fSorter]
        
        let managedObjectContext = api.managedObjectContext
        do {
            if let fResults = try managedObjectContext.fetch(fReq) as? [Datasets] {
                for objToDelete: NSManagedObject in fResults {
                    managedObjectContext.delete(objToDelete)
                }
            }
        } catch {
            print("delete \(entityName)/\(nome) error: \(error)")
        }

    }
    
    func updateCoreData() {
        deleteFromCoreData()
        
        guard let nome = self.nome
            , let uri = self.uri
            , let version = self.version
            , let estimatedSize = self.estimated_size
        else { return }
        
        let managedObjectContext = api.managedObjectContext
        
        let coreDataEntity = NSEntityDescription.insertNewObject(forEntityName: "Datasets", into: managedObjectContext) as! Datasets
        coreDataEntity.nome = nome
        coreDataEntity.uri = uri
        coreDataEntity.version = version
        coreDataEntity.estimatedSize = Int32(estimatedSize)
        
        do {
            try managedObjectContext.save()
            print("\(uri) salvato!")
        } catch {
            print("errore managedObjectContext: \(error)")
        }

    }

}
