//
//  CoreDataController.swift
//
//  Created by Repower on 10/09/17.
//  Copyright © 2017 Repower. All rights reserved.
//

import Foundation
import CoreData
import UIKit

var coreDataContext: NSManagedObjectContext?
/*
 La classe CoreDataController è stata creata con l'unico scopo di velocizzare l'interazione con le funzioni del Core Data.
 Qui dentro troverai le funzioni necessarie al salvataggio, eliminazione ecc degli elementi salvati in memoria.
 */

class CoreDataController {
    static let shared = CoreDataController() // proprietà per ottenere la classe in modalità Singleton
    
    public var context: NSManagedObjectContext // riferimento al contenitore degli oggetti (ManagedObject) salvati in memoria
    
    private init() {
        //        let application = UIApplication.shared.delegate as! AppDelegate // recupero l'istanza dell'AppDelegate dell'applicazione
        //        self.context = application.persistentContainer.viewContext // recupero il ManagedObjectContext dalla proprietà persistantContainer presente nell'App Delegate
        
        self.context = coreDataContext!
    }
    
    func addJson(idUserLogged: Int32, data: Data, url: String, dataAggiornamento: Date = Date()) {
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Json", in: self.context) else {
            print(#function, "errore nella creazione dell'entity")
            return
        }
        let newJson = Json(entity: entity, insertInto: self.context)
        newJson.idUserLogged = Int32(idUserLogged)
        newJson.data = data
        newJson.url = url
        newJson.dateLastRetrive = dataAggiornamento
        
        do {
            try self.context.save() // la funzione save() rende persistente il nuovo oggetto in memoria
        } catch let error {
            print("""
                Errore salvataggio Json \(newJson.url) in memoria:
                \(error)
                """)
        }
    }
    
    func deleteCachedData(_ entityName: String, onlyForIdUserLogged: Bool = true) -> Bool {
        let fReq:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        
        if onlyForIdUserLogged {
            let fPred = NSPredicate(format: "(idUserLogged = %d)", idUserLogged)
            fReq.predicate = fPred
        }
        do {
            if let fResults = try self.context.fetch(fReq) as? [NSManagedObject] {
                print("DELETE cache of '\(entityName)'")
                for objToDelete: NSManagedObject in fResults {
                    self.context.delete(objToDelete)
                }
                do {
                    try self.context.save() // save() rende persistente la cancellazione
                    return true
                } catch let error {
                    print("Errore cancellazione \(entityName): \(error)")
                    return false
                }
            }
        }
        catch {
            print("deleteCachedData Error: \(error)")
            return false
        }
        return false
    }
    
    func getLastCachedJson(urlPathComplete url: String)  -> [Json]? {
        var jsons: [Json]? = []
        let fReq:NSFetchRequest<NSFetchRequestResult> = Json.fetchRequest()
        
        let fPred1 = NSPredicate(format: "(url = %@)", url)
        //        print("idUserLogged = \(idUserLogged) isLoggedIn = \(isLoggedIn)")
        let fPred2 = NSPredicate(format: "(idUserLogged = %d)", idUserLogged)
        
        // Combine the two predicates above in to one compound predicate
        let fPred = NSCompoundPredicate(type: .and, subpredicates: [fPred1, fPred2])
        fReq.predicate = fPred
        
        let fSorter: NSSortDescriptor = NSSortDescriptor(key: "dateLastRetrive", ascending: false)
        fReq.sortDescriptors = [fSorter]

        
        do {
            jsons = try context.fetch(fReq) as? [Json]
            return jsons
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        return nil
    }
}

