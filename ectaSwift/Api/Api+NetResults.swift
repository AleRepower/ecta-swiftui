
//  Api+Results.swift
//  Net
//
//  Created by AD'A on 24/06/17.
//  Copyright © 2017 Repower. All rights reserved.
//


import UIKit
import CoreSpotlight
import MobileCoreServices


extension Api  {
    
    @objc func didReceiveAPIResults(_ notification: Notification) {
        guard let userInfo = notification.userInfo as? Api.UserInfo else { return }
        
        guard let success = userInfo["success"] as? Bool
            , let method = userInfo["method"] as? String
//            , let dataResult = userInfo["data"] as? Data
//            , let date = userInfo["date"] as? Date
            else { return }
        
        let jsonResult = userInfo["json"] as? NSDictionary
        
        if method == "logout" {
//            self.cleanAll()
        }
        
        // debug
        NSLog("API: end \(method)")
        
        guard success else {
            NSLog("API: end with failure: \(method)")
            return
        }
        
        if let idUser = jsonResult?["id_consulente"] as? Int, method == "login" {
            api.isUserAlreadyLogged(idUser: idUser)
            
            // Background Fetch
//            UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
            
            DispatchQueue.global(qos: .background).async {
                api.register()
            }
        }
        
        if method == "struttura" {
            // devo passare tutto "solo" per Spotlight
            var consulenti = [Consulente]()
            let jsonConsulenti: [NSDictionary] = jsonResult!["consulenti"] as! [NSDictionary]
            
            for jsonConsulente: NSDictionary in jsonConsulenti {
                
                // recupero i contacts
                let jsonContattiConsulente: [NSDictionary] = jsonConsulente["contacts"] as! [NSDictionary]
                
                var contattiConsulente: [Recapiti] = []
                for jsonContattoConsulente: NSDictionary in jsonContattiConsulente {
                    let tipo: String? = (jsonContattoConsulente["tipo"] as! String)
                    let etichetta: String? = (jsonContattoConsulente["etichetta"] as! String)
                    let valore: String? = (jsonContattoConsulente["valore"] as! String)
                    let latitudine: Double? = jsonContattoConsulente["latitudine"] as! NSObject != NSNull() ? (jsonContattoConsulente["latitudine"] as! Double) : nil
                    let longitudine: Double? = jsonContattoConsulente["latitudine"] as! NSObject != NSNull() ? (jsonContattoConsulente["longitudine"] as! Double) : nil
                    
                    let newContatto = Recapiti(tipo: tipo, etichetta: etichetta, valore: valore, latitude: latitudine, longitude: longitudine, distance_from_POI: nil)
                    
                    contattiConsulente.append(newContatto)
                }
                
                // recupero la gerarchia
                let jsonGerarchiaConsulente: [NSDictionary] = jsonConsulente["gerarchia"] as! [NSDictionary]
                
                var gerarchiaConsulente: [Livello] = []
                for jsonLivelloConsulente: NSDictionary in jsonGerarchiaConsulente {
                    let id_livello: Int? = (jsonLivelloConsulente["id_livello"] as! Int)
                    let livello: String? = (jsonLivelloConsulente["livello"] as! String)
                    let descrizione: String? = (jsonLivelloConsulente["descrizione"] as! String)
                    
                    // recupero il responsabile
                    let jsonResponsabileLivello: NSDictionary = jsonLivelloConsulente["responsabile"] as! NSDictionary
//                    var responsabileLivello: [Responsabile] = []
                    let id_consulente: Int? = (jsonResponsabileLivello["id_consulente"] as! Int)
                    let cognome: String? = (jsonResponsabileLivello["cognome"] as! String)
                    let nome: String? = (jsonResponsabileLivello["nome"] as! String)
                    let sesso: String? = (jsonResponsabileLivello["sesso"] as! String)
                    let email: String? = (jsonResponsabileLivello["email"] as! String)
                    let tel_commerciale: String? = (jsonResponsabileLivello["tel_commerciale"] as! String)
                    
                    let newResponsabile = Responsabile(id_consulente: id_consulente, cognome: cognome, nome: nome, sesso: sesso, email: email, tel_commerciale: tel_commerciale)
//                    responsabileLivello.append(newResponsabile)
                    
                    let newLivello = Livello(id_livello: id_livello, livello: livello, descrizione: descrizione, responsabile: newResponsabile)
                    gerarchiaConsulente.append(newLivello)
                }
                
                
                // recupero gli altri dati del consulente
                let cognome = (jsonConsulente["cognome"] as? String) ?? ""
                let data_formazione = (jsonConsulente["data_formazione"] as? String) ?? ""
                let data_inizio_attivita = (jsonConsulente["data_inizio_attivita"] as? String) ?? ""
                let data_ultima_firma = (jsonConsulente["data_ultima_firma"] as? String) ?? ""
                let data_nascita = (jsonConsulente["data_nascita"] as? String) ?? ""
                let id_consulente = (jsonConsulente["id_consulente"] as? Int) ?? 0
                let limite_prenotazioni = (jsonConsulente["limite_prenotazioni"] as? Int) ?? 0
                let nome = (jsonConsulente["nome"] as? String) ?? ""
                let pic_hash = (jsonConsulente["pic_hash"] as? String) ?? ""
                let prenotazioni = (jsonConsulente["prenotazioni"] as? Int) ?? 0
                let clienti = (jsonConsulente["clienti"] as? Int) ?? 0
                let qualifica = (jsonConsulente["qualifica"] as? String) ?? ""
                let sesso = (jsonConsulente["sesso"] as? String) ?? ""
                let ultimo_cambio_password = (jsonConsulente["ultimo_cambio_password"] as? String) ?? ""
                let ultimo_login = (jsonConsulente["ultimo_login"] as? String) ?? ""
                let contacts = contattiConsulente
                let livelli = gerarchiaConsulente
                
                let newConsulente = Consulente(
                    cognome: cognome
                    ,data_formazione: data_formazione
                    ,data_inizio_attivita: data_inizio_attivita
                    ,data_ultima_firma: data_ultima_firma
                    ,data_nascita: data_nascita
                    ,id_consulente: id_consulente
                    ,limite_prenotazioni: limite_prenotazioni
                    ,nome: nome
                    ,pic_hash: pic_hash
                    ,prenotazioni: prenotazioni
                    ,clienti: clienti
                    ,qualifica: qualifica
                    ,sesso: sesso
                    ,ultimo_cambio_password: ultimo_cambio_password
                    ,ultimo_login: ultimo_login
                    ,contacts: contacts
                    ,livelli: livelli
                )
                consulenti.append(newConsulente)
            }
            
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.badge]) { (granted, error) in
                // Enable or disable features based on authorization.
            }
            
            DispatchQueue.global(qos: .background).async {
                self.setSpotLight(consulenti: consulenti)
            }
            
            DispatchQueue.main.async {
                UIApplication.shared.applicationIconBadgeNumber = consulenti.count > 1 ? consulenti.count : 0
            }
        }
        
    }
    
    func setSpotLight(consulenti: [Consulente]) {
        
        if #available(iOS 9.0, *) {
            NSLog("It's Spotlight time")
            
            CSSearchableIndex.default().deleteSearchableItems(withDomainIdentifiers: [domainIdentifier], completionHandler: { error in
                if let error = error {
                    print("Remove error: \(error.localizedDescription)")
                }
            })
            
//            // rimuovo tutte le notifiche compleanno pending
//            let center = UNUserNotificationCenter.current()
//            center.removePendingNotificationRequests(withIdentifiers: [birthdayIdentifier])
            
            for consulente in consulenti {
                
                let itemIdentifier = String(consulente.id_consulente)
                
                let attributeSet = CSSearchableItemAttributeSet( itemContentType: kUTTypeContact as String)
                attributeSet.displayName = "\(consulente.nome) \(consulente.cognome)"
                // attributeSet.contentDescription = "\(consulente.natoDa().desc), in Repower da \(consulente.inAziendaDa().desc)\nUltimo accesso a Porto/Apporto: \(consulente.ultimoAccessoPortoDa().desc)"
                
                attributeSet.accountIdentifier = String(consulente.id_consulente)
                attributeSet.contactKeywords = [String(consulente.id_consulente)]
                
                if let dataNascita = consulente.dataNascita() {
                    attributeSet.importantDates?.append(dataNascita)
                    
//                    // aggiungo notifica compleanno
//                    let todayComponents = Calendar.current.dateComponents([.month, .day], from: Date())
//                    let dataNascitaComponents = Calendar.current.dateComponents([.month, .day], from: dataNascita)
//                    if todayComponents.month == dataNascitaComponents.month && todayComponents.day == dataNascitaComponents.day {
//                        self.sendBirthDayLocalNotification(title: attributeSet.displayName ?? "", body: "\(dataNascita)")
//                    }
                }
                
                var contentDescription = "\(consulente.natoDa().desc), in Repower da \(consulente.inAziendaDa().desc)."
                let manager = consulente.livelli[0].responsabile
                if let managerCognome = manager.cognome, let managerNome = manager.nome, let idConsulenteManager = manager.id_consulente {
                    let qualifica = ""
//                    let qualifica = consulente.qualificaAttr().desc
//                    attributeSet.keywords?.append(qualifica)
                    let managerDescription = idConsulenteManager == consulente.id_consulente ? "\(qualifica)" : "manager: \(managerNome) \(managerCognome)"
                    contentDescription = "\(contentDescription)\nCodice \(consulente.id_consulente), \(managerDescription)."
                }
                attributeSet.contentDescription = contentDescription
                attributeSet.creator = "Net by Repower"

                attributeSet.contentModificationDate = Date()
                attributeSet.supportsPhoneCall = false
                
                let item = CSSearchableItem(uniqueIdentifier: itemIdentifier, domainIdentifier: domainIdentifier, attributeSet: attributeSet)
                
                CSSearchableIndex.default().indexSearchableItems([item], completionHandler: { error in
                    if let error =  error {
                        print("CSSearchableIndex error: \(error.localizedDescription)")
                    }
                })
            }
        }
    }
    
//    func sendBirthDayLocalNotification(_ consulente: Consulente){
//
//        let content = UNMutableNotificationContent()
//        content.title = "Compleanno di \(consulente.nome) \(consulente.cognome)"
//        content.subtitle = "nato \(consulente.natoDa().anni) anni"
//        content.body = ""
//        content.sound = UNNotificationSound.default
//        content.userInfo = ["consulente": consulente]
//
//        //To Present image in notification
//        if let path = Bundle.main.path(forResource: "monkey", ofType: "png") {
//            let url = URL(fileURLWithPath: path)
//
//            do {
//                let attachment = try UNNotificationAttachment(identifier: "sampleImage", url: url, options: nil)
//                content.attachments = [attachment]
//            } catch {
//                print("attachment not found.")
//            }
//        }
//
//        // Deliver the notification
//        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1.0, repeats: false)
//        let request = UNNotificationRequest(identifier: birthdayIdentifier, content: content, trigger: trigger)
//
////        UNUserNotificationCenter.current().delegate =
//        UNUserNotificationCenter.current().add(request){(error) in
//            if (error != nil){
//                print(error?.localizedDescription ?? "")
//            }
//        }
//    }
}
