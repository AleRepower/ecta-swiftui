//
//  ApiApporto.swift
//  Apporto
//
//  Created by AD'A on 25/05/17.
//  Copyright © 2017 Repower. All rights reserved.
//


import CoreData
import CoreLocation

extension Api {
    
    // MARK: - Autentication
    func ssoToken(params: Params, extraInfo: ExtraInfo? = nil){
        let method = "sso_token"
        call(method, httpMethod: .POST, params: params, extraInfo: extraInfo)
    }
    
    func loginApporto(params: Params, handler: ( (UserInfo?) -> Void )? = nil) -> Bool {
        let method = "login"
        
        let vendorID = api.appUtilities.getDeviceInfo().uuid ?? ""
//        let app_name = api.appUtilities.getAppInfo().name ?? ""
        let app_name = "Net"
        let app_version = api.appUtilities.getAppInfo().version ?? ""
        let os_family = api.appUtilities.getDeviceInfo().osFamily ?? ""
        let os_version = api.appUtilities.getDeviceInfo().osVersion ?? ""
        let lat = api.appUtilities.getGeoInfo().latitude
        let lon = api.appUtilities.getGeoInfo().longitude
        
        var params = params
        
        if isLoggedIn {
            let userInfo = ["success":true, "method": method, "date": Date()] as [String : Any]
            NotificationCenter.default.post(name: Notification.Name(rawValue: kRepowerApiNotification), object: self, userInfo: userInfo)
            if let handler = handler { handler(userInfo) }
            return true
        }
        
        // controllo prima i parametri obbligatori
        guard let _ = params["username"], let _ = params["password"], !vendorID.isEmpty, !app_name.isEmpty, !app_version.isEmpty else {
            api.appUtilities.showAlert("Qualcosa è andato storto con il recupero di dati critici. Non posso continuare.", titleMessage: "Attenzione", titleCancelButton: "OK")
            return false
        }
        
        params["vendorID"] = vendorID
        params["app_name"] = app_name
        params["app_version"] = app_version
        
        if !os_family.isEmpty && !os_version.isEmpty {
            params["os_family"] = os_family
            params["os_version"] = os_version
        }
        
        if let lat = lat, let lon = lon {
            params["lat"] = lat
            params["lon"] = lon
        }
        
        call(method, httpMethod: .POST, params: params, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
        return true
    }
    
    func userProfile() {
        let method = "user/profile"
        self.call(method)
    }
    
    
    func configuration() {
        let method = "configuration"
        call(method)
    }
    
    func register() {
        let method = "register"
        
        let params: Params = [
            "os_family" : appUtilities.getDeviceInfo().osFamily ?? "",
            "os_version" : appUtilities.getDeviceInfo().osVersion ?? "",
            "hw_manufacturer" : "Apple",
            "hw_model" : appUtilities.getDeviceInfo().model ?? "",
            "hw_id_sha1" : appUtilities.getDeviceInfo().uuid ?? "",
            "app_name" : "Apporto",
            "app_version" : appUtilities.getAppInfo().version ?? "", //"1.3",
            "ui_lang_iso" : Locale.current.languageCode ?? "",
            "ui_format_iso" : Locale.current.regionCode ?? "",
            "vendorID": appUtilities.getDeviceInfo().uuid ?? "",
            //"apn_token_auth" : apnToken,
        ]
        call(method, httpMethod: .POST, params: params)
    }
    
    @objc func logoutApporto() {
        let method = "logout"
        call(method, httpMethod: .POST)
    }
    
    // MARK: - Dataset
    func datasetsUris() {
        //        let method = "datasets/uris"
        let method = "datasets/uris_new"
        call(method)
    }
    
    func datasetClassiProfittabilita(method: String = "datasets/classi_profittabilita") {
        call(method)
    }
    
    func datasetComuni(method: String = "datasets/comuni") {
        call(method)
    }
    
    func datasetFatturati(method: String = "datasets/fatturato") {
        call(method)
    }
    
    func datasetFormeGiuridiche(method: String = "datasets/forme_giuridiche") {
        call(method)
    }
    
    func datasetNumeriDipendenti(method: String = "datasets/numero_dipendenti") {
        call(method)
    }
    
    func datasetProvince(method: String = "datasets/province") {
        call(method)
    }
    
    func datasetRegioni(method: String = "datasets/regioni") {
        call(method)
    }
    
    func datasetSettoriMerceologici(method: String = "datasets/settori_merceologici") {
        call(method)
    }
    
    func datasetStatiTrattativa(method: String = "datasets/stati_trattativa") {
        call(method)
    }
    
    // MARK: - Playlist
    func playlistPrefilter(params: Params){
        let method = "playlist/prefilter"
        call(method, params: params)
    }
    
    func playlistFilter(params: Params){
        let method = "playlist/filter"
        call(method, params: params)
    }
    
    func playlistContatti(params: Params){
        let method = "playlist/contatti"
        call(method, params: params)
    }
    
    func playlistFilters(handler: ( (UserInfo?) -> Void )? = nil){
        let method = "playlist/filters"
        call(method, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
    }
    
    func playlistFiltersSave(params: Params, handler: ( (UserInfo?) -> Void )? = nil ){
        let method = "playlist/filters/save"
        call(method, httpMethod: .POST, params: params, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
    }
    
    func playlistFiltersDelete(params: Params, handler: ( (UserInfo?) -> Void )? = nil ){
        let method = "playlist/filters/delete"
        call(method, httpMethod: .POST, params: params, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
    }
    
    func playlistFiltersRename(params: Params, handler: ( (UserInfo?) -> Void )? = nil ){
        let method = "playlist/filters/rename"
        call(method, httpMethod: .POST, params: params, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
    }
    
    func downloadRicercheSuggerite(handler: ( ( Bool ) -> () )? = nil ) {
        print("Ricerche suggerite url:", URL_JSON_RICERCHESUGGERITE)
        guard let url = URL(string: URL_JSON_RICERCHESUGGERITE) else { return }
        let config = URLSessionConfiguration.default
        //        config.requestCachePolicy = .reloadIgnoringLocalCacheData
        let session = URLSession.init(configuration: config)
        let task = session.dataTask(with: url) { (data, response, error)  in
            guard let data = data, error == nil else {
                print("Ricerche suggerite error:",error.debugDescription)
                if let handler = handler { handler(false) }
                return
            }
            UserDefaults.standard.set(data, forKey: USERDEFAULT_KEY_RICERCHESUGGERITE)
            let dataVer = UserDefaults.standard.data(forKey: USERDEFAULT_KEY_RICERCHESUGGERITE)
            print("dataVer", dataVer as Any)
            
            if let handler = handler { handler(true) }
        }
        task.resume()
    }
    
    // MARK: - Azienda
    func aziendaRicerca(params: Params){
        
        //        latitude
        //        longitude
        //        lat_sort
        //        lon_sort
        //        path
        //        ragione_sociale
        //        p_iva
        //        tipologia
        //        poi
        //        sections
        //        ownership
        
        //        let method = "azienda/ricercanewJSON"
        
        // PER PROD
//        let method = "azienda/ricercanew"
//        call(method, HTTPMethod: "GET", params: params)
        
        // PER DEBUG
//        let noCache = false
        let method = "azienda/ricercanew"
        let api = { self.call(method, params: params) }
        
        // cache time
        let paramsString = paramsToString(params)
        let urlPathComplete = !paramsString.isEmpty ? "\(kBasePath)\(method)/?\(paramsString)" : "\(kBasePath)\(method)/"
        
        guard let jsons = PersistenceController.shared.getLastCachedJson(urlPathComplete: urlPathComplete), let json = jsons.first else {
            print("Nessun json cachato! chiamo il metodo \(method)")
            api()
            return
        }
        
//        let deleteOldCache = {
//            for jsonToDelete in jsons where jsonToDelete != jsons.first {
//                self.managedObjectContext.delete(jsonToDelete)
//            }
//        }
//        if !noCache {
        let cachedData = json.data! as Data
        let dataAggiornamento = json.dateLastRetrive! as Date
            let userInfo: UserInfo = ["success":true
                , "method": method
                , "data": cachedData
                , "date": dataAggiornamento
                //                , "extraInfo": extraInfo
            ]
            goWithJSON(userInfo, handler: nil)
//        }
//        else { //non mi basta - prima svuoto (tranne il più recente che mantengo per sicurezza) e poi recupero i dati dal metodo
//            deleteOldCache()
//            api()
//        }
        
        //        let fResults: [Json]? = CoreDataController.shared.getLastCachedJson(urlPathComplete)
        //
        //        if fResults == nil || fResults!.count == 0 {
        //            print("no matches! chiamo il metodo \(method)")
        //            api()
        //        } else {
        //            if !noCache {
        //                let myJson = fResults![0] as Json
        //                let cachedData = myJson.data as Data
        //                let dataAggiornamento = myJson.dateLastRetrive as Date
        //
        //                goWithJSON(method, data: cachedData, date: dataAggiornamento)
        //            } else { //non mi basta - prima svuoto e poi recupero i dati dal metodo
        //                for objToDelete: Json in fResults! {
        //                    managedObjectContext.delete(objToDelete)
        //                }
        //                api()
        //            }
        //        }
    }
    
    func aziendaAroundMe(params: Params) {
        let method = "azienda/aroundme"
        call(method, params:params)
    }
    func azienda(idAzienda: Int) {
        let params: Params = [
            "id_azienda": String(idAzienda),
            "sections": "*"
        ]
        self.azienda(params)
    }
    func azienda(_ params: Params) {
        let method = "azienda"
        call(method, params: params)
    }
    
    func aziende(_ params: Params) {
        //        let locationManager = CLLocationManager()
        //        let currentLocation = locationManager.location?.coordinate
        //        if let currentLocation = currentLocation {
        //            params["latitude"] = String(currentLocation.latitude)
        //            params["longitude"] = String(currentLocation.longitude)
        //        }
        
        let method = "aziende"
        call(method, params: params)
    }
    
    func aziendaBook(_ params: Params) {
        let method = "azienda/book"
        call(method, httpMethod: .POST, params:params)
    }
    
    func aziendaStorico(_ params: Params) {
        let method = "azienda/storico"
        call(method, params:params)
    }
    
    func aziendaPicture(params: Params, extraInfo: ExtraInfo? = nil) {
        let method = "azienda/picture"
        call(method, params:params, contentType: "application/octet-stream", extraInfo: extraInfo)
    }
    
    func aziendaPictureSave(params: Params, imageToUpload: Data) {
        let method = "azienda/picture/save"
        let contentType = "image/jpeg" //"application/octet-stream"
        call(method, httpMethod: .POST, params: params, contentType: contentType, imageToUpload: imageToUpload)
    }
    
    func aziendaPictureDelete(params: Params) {
        let method = "azienda/picture/delete"
        call(method, params: params)
    }
    
    func setAziendaStatoTrattativa(params: Params){
        let method = "azienda/set_stato_trattativa"
        call(method, httpMethod: .POST, params: params)
    }
    
    func aziendaReferenze(params: Params) {
        let method = "azienda/referenze"
        call(method, params: params)
    }
    
    func aziendaReferenzaNew(params: Params) {
        let method = "azienda/referenza/new"
        call(method, httpMethod: .POST, params: params)
    }
    
    func aziendaReferenzaDelete(params: Params) {
        let method = "azienda/referenza/delete"
        call(method, httpMethod: .POST, params: params)
    }
    
    func requestCreditCheck(params: [String : String]) {
        let method = "azienda/credit_check/request"
        call(method, httpMethod: .POST, params: params)
    }
    
    // MARK: - Consulente
    func consulentePicture(params: Params, noCache: Bool = false) {
        let method = "consulente/picture"
        let extraInfo = params
        let api = { self.call(method, params:params, contentType: "application/octet-stream", extraInfo: extraInfo) }
        
        // cache time
        let paramsString = paramsToString(params)
        let urlPathComplete = !paramsString.isEmpty ? "\(kBasePath)\(method)/?\(paramsString)" : "\(kBasePath)\(method)/"
        
        guard let jsons = PersistenceController.shared.getLastCachedJson(urlPathComplete: urlPathComplete), let json = jsons.first else {
            print("Nessun json cachato! chiamo il metodo \(method)")
            api()
            return
        }
        
        let deleteOldCache = {
            for jsonToDelete in jsons where jsonToDelete != jsons.first {
                self.managedObjectContext.delete(jsonToDelete)
            }
        }
        if !noCache {
            let cachedData = json.data! as Data
            let dataAggiornamento = json.dateLastRetrive! as Date
            let userInfo: UserInfo = ["success":true
                , "method": method
                , "data": cachedData
                , "date": dataAggiornamento
                //                , "extraInfo": extraInfo
            ]
            goWithData(userInfo, handler: nil)
        } else { //non mi basta - prima svuoto (tranne il più recente che mantengo per sicurezza) e poi recupero i dati dal metodo
            deleteOldCache()
            api()
        }
    }
    
    func consulenteStats(params: Params = [:], noCache: Bool = false) {
        let method = "consulente/stats"
        let api = { self.call(method, params:params) }
        
        // cache time
        let paramsString = paramsToString(params)
        let urlPathComplete = !paramsString.isEmpty ? "\(kBasePath)\(method)/?\(paramsString)" : "\(kBasePath)\(method)/"
        
        guard let jsons = PersistenceController.shared.getLastCachedJson(urlPathComplete: urlPathComplete), let json = jsons.first else {
            print("Nessun json cachato! chiamo il metodo \(method)")
            api()
            return
        }
        
        let deleteOldCache = {
            for jsonToDelete in jsons where jsonToDelete != jsons.first {
                self.managedObjectContext.delete(jsonToDelete)
            }
        }
        if !noCache {
            let cachedData = json.data! as Data
            let dataAggiornamento = json.dateLastRetrive! as Date
            let userInfo: UserInfo = ["success":true
                , "method": method
                , "data": cachedData
                , "date": dataAggiornamento
                //                , "extraInfo": extraInfo
            ]
            goWithJSON(userInfo, handler: nil)
            
            // verifico se recuperare comunque un dato aggiornato
            DispatchQueue.global(qos: .background).async {
                let calendar: Calendar = Calendar.current
                let today = calendar.startOfDay(for: Date())
                let dateUpdate = calendar.startOfDay(for: dataAggiornamento)
                
                let days = calendar.compare(dateUpdate, to: today, toGranularity: .day)
                let isTimeToRefresh =  days != ComparisonResult.orderedSame
                
                if isTimeToRefresh {
                    print("isTimeToRefresh `consulente/stats", isTimeToRefresh, days, ComparisonResult.orderedSame)
                    deleteOldCache()
                    api()
                }
            }
        } else { //non mi basta - prima svuoto (tranne il più recente che mantengo per sicurezza) e poi recupero i dati dal metodo
            deleteOldCache()
            api()
        }
    }
    
    // MARK: - Struttura
    func struttura(_ noCache: Bool = false) {
        let method = "struttura"
        let api = { self.call(method) }
        
        // cache time
        let paramsString = paramsToString([:])
        let urlPathComplete = !paramsString.isEmpty ? "\(kBasePath)\(method)/?\(paramsString)" : "\(kBasePath)\(method)/"

        guard let jsons = PersistenceController.shared.getLastCachedJson(urlPathComplete: urlPathComplete), let json = jsons.first else {
            print("Nessun json cachato! chiamo il metodo \(method)")
            api()
            return
        }
        
        let deleteOldCache = {
            for jsonToDelete in jsons where jsonToDelete != jsons.first {
                self.managedObjectContext.delete(jsonToDelete)
            }
        }
        if !noCache {
            let cachedData = json.data! as Data
            let dataAggiornamento = json.dateLastRetrive! as Date
            let userInfo: UserInfo = ["success":true
                , "method": method
                , "data": cachedData
                , "date": dataAggiornamento
                //                , "extraInfo": extraInfo
            ]
            goWithJSON(userInfo, handler: nil)
            
            // verifico se recuperare comunque un dato aggiornato
            DispatchQueue.global(qos: .background).async {
                let calendar: Calendar = Calendar.current
                let today = calendar.startOfDay(for: Date())
                let dateUpdate = calendar.startOfDay(for: dataAggiornamento)
                
                let days = calendar.compare(dateUpdate, to: today, toGranularity: .day)
                let isTimeToRefresh =  days != ComparisonResult.orderedSame
                
                if isTimeToRefresh {
                    print("isTimeToRefresh `consulente/struttura", isTimeToRefresh, days, ComparisonResult.orderedSame)
                    deleteOldCache()
                    api()
                }
            }
        } else { //non mi basta - prima svuoto (tranne il più recente che mantengo per sicurezza) e poi recupero i dati dal metodo
            deleteOldCache()
            api()
        }
    }
    
}
