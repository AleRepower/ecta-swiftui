//
//  Api+ECTA.swift
//  ECTASwift
//
//  Created by Alessandro Grigiante on 18/10/2021.
//  Copyright © 2020 Repower. All rights reserved.
//

import CoreData
import CoreLocation

var idUtente: Int = 0
var theUsername: String?




var aziendaDefault: AziendaDefault?

extension Api {
    
    // MARK: - Autentication
//    func ssoToken(params: Params, extraInfo: ExtraInfo? = nil){
//        let method = "sso_token"
//        call(method, httpMethod: .POST, params: params, extraInfo: extraInfo)
//    }
    
    func loginECTA(params: Params, handler: ( (UserInfo?) -> Void )? = nil) -> Bool {
        let method = URL_POST_LOGIN
        
        let vendorID = api.appUtilities.getDeviceInfo().uuid ?? ""
//        let app_name = api.appUtilities.getAppInfo().name ?? ""
        let app_name = "ECTA"
        let app_version = api.appUtilities.getAppInfo().version ?? ""
//        let os_family = api.appUtilities.getDeviceInfo().osFamily ?? ""
//        let os_version = api.appUtilities.getDeviceInfo().osVersion ?? ""
        let lat = api.appUtilities.getGeoInfo().latitude
        let lon = api.appUtilities.getGeoInfo().longitude
        
        var params = params
        
        if isLoggedIn {
            let userInfo = ["success":true, "method": method, "date": Date()] as [String : Any]
            NotificationCenter.default.post(name: Notification.Name(rawValue: kRepowerApiNotification), object: self, userInfo: userInfo)
            if let handler = handler { handler(userInfo) }
            return true
        }
        
        // controllo prima i parametri obbligatori
        guard let _ = params["username"], let _ = params["password"], !vendorID.isEmpty, !app_name.isEmpty, !app_version.isEmpty else {
            api.appUtilities.showAlert("Qualcosa è andato storto con il recupero di dati critici. Non posso continuare.", titleMessage: "Attenzione", titleCancelButton: "OK")
            return false
        }
//        self.userName = params["username"] ?? "Username not found!"
        params["vendorID"] = vendorID
        params["appVersion"] = "2.2"
        
//        params["app_name"] = app_name
//        params["app_version"] = app_version
//
//        if !os_family.isEmpty && !os_version.isEmpty {
//            params["os_family"] = os_family
//            params["os_version"] = os_version
//        }
        
        if let lat = lat, let lon = lon {
            params["lat"] = lat
            params["lon"] = lon
        }else{
            params["lat"] = "0"
            params["lon"] = "0"
        }
//        FORCE PARAMS RESET DBG ONLY!
//        let params2 = ["": ""]
        
        call(method, httpMethod: .POST, params: params, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
        return true
    }
    
    
    func logoutECTA(method: String = URL_POST_LOGOUT) {
        let params: [String:String] = ["username" : theUsername!]
        call(method, httpMethod: .GET)
    }
    
    
    // MARK: - Dataset ECTA
//    DataSet Fissi
    func datasetComuniECTA(method: String = URL_GET_COMUNI) {
        call(method)
    }
    
    func datasetsTipiDocumento(method: String = URL_GET_TIPI_DOCUMENTO) {
        call(method)
    }
    
    func datasetTipiAttivita(method: String = URL_GET_TIPI_ATTIVITA) {
        call(method)
    }
    
    func datasetContrattiTrasporto(method: String = URL_GET_TIPI_DOCUMENTO_TRASPORTO) {
        call(method)
    }
    
    func datasetChanges(method: String = URL_GET_DATASET_CHANGES) {
        call(method)
    }
    
    func datasetStatiPagamento(method: String = URL_GET_DATASET_STATI_PAGAMENTO) {
        call(method)
    }
    
//Chiamate Get Dinamiche (params: Params, handler: ( (UserInfo?) -> Void )? = nil) -> Bool {

    
    func datasetAziendaInfo(params: Params, handler: ( (UserInfo?) -> Void )? = nil) -> Bool {
        let method = URL_GET_AZIENDA_INFO
        call(method, params: params, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
        return true
    }
    
    func genericEctaGetDataSet(method: String, params: Params = [:], handler: ( (UserInfo?) -> Void )? = nil) -> Bool {
        call(method,  httpMethod: .GET, params: params, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
        return true
    }
    
    func genericEctaPostDataSet(method: String, params: Params = [:], handler: ( (UserInfo?) -> Void )? = nil) -> Bool {
        call(method,  httpMethod: .POST, params: params, handler: { (userInfo) in
            guard let handler = handler else { return }
            handler(userInfo)
        })
        return true
    }
    
    func setAziendaDefault(method: String = URL_SET_AZIENDA_DEFAULT) {
        call(method)
    }
    
    func datasetLetture(method: String = URL_GET_LETTURE) {
        call(method)
    }
    
    func datasetInfoDatiAutolettura(method: String = URL_GET_INFO_DATI_AUTOLETTURA) {
        call(method)
    }
    
    func datasetCheckSendLettureGas(method: String = URL_GET_DATE_AUTOLETTURA) {
        call(method)
    }
    
    func datasetCercaAzienda(method: String = URL_CERCA_AZIENDE) {
        call(method)
    }
    
    
    @objc func didReceiveAPIResults(_ notification: Notification) {
        guard let userInfo = notification.userInfo as? Api.UserInfo else { return }
        
        guard let success = userInfo["success"] as? Bool
            , let method = userInfo["method"] as? String
//            , let dataResult = userInfo["data"] as? Data
//            , let date = userInfo["date"] as? Date
            else { return }
        
        let jsonResult = userInfo["json"] as? NSDictionary
        
        if method == URL_POST_LOGOUT {
            self.cleanAll()
            return
        }
        
        // debug
        NSLog("API: end \(method)")
        
        guard success else {
            NSLog("API: end with failure: \(method)")
            return
        }
        
        if let idUser = jsonResult?["idUtente"] as? Int, method == URL_POST_LOGIN {
            api.isUserAlreadyLogged(idUser: idUser)
            
            // Background Fetch
//            UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
//            DispatchQueue.global(qos: .background).async {
//                api.register()
//            }
            
        }
    }
}
