//
//  Api.swift
//  ECTA
//
//  Based on by AD'A on 31/07/14.
//  Refactored by Alessandro Grigiante on 18/10/2021.
//  Copyright © 2021 Repower. All rights reserved.

import CoreData
import Foundation
//import CDAlertView
//import SwiftKeychainWrapper
import Reachability

@objc protocol RepowerApiProtocol {
    func didReceiveAPIResults(_ notification: Notification)
}


let kRepowerApiNotification = "RepowerApiNotification"
var isLoggedIn = false
var idUserLogged = Int()

let api = Api.shared

enum HttpMethod: String {
    case GET = "GET"
    case POST = "POST"
    //    case PUT = "PUT"
    //    case DELETE = "DELETE"
}

// MARK: - ECTA API class
class Api: NSObject, URLSessionDelegate{
    
    static let shared = Api()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        return PersistenceController.preview.container.viewContext
    }()
    
    typealias Params = [String: String]
    typealias UserInfo = [String: Any]
    typealias ExtraInfo = [String: Any]
    
    let appUtilities: Utilities = Utilities()
    
//    PROD or DEV endpoint
//    let kBasePath = ECTA_BASE_PATH
    let kBasePath = ECTA_BASE_PATH_DEV
    
//    var currentDatasets = [CurrentDatasets]()
//    var userId = String()
    let deviceKey = String()
    let authSecret = String()
//    var userName = String()
    
    var isReachable: Bool = true
    var isReachableWiFi: Bool = false
    var isAlertMsgReachableShowing: Bool = false
    
    var reachability: Reachability?
    
    override init(){
        super.init()
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.didReceiveAPIResults(_:)), name: NSNotification.Name(rawValue: kRepowerApiNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(_:)), name: Notification.Name.reachabilityChanged, object: reachability)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.logoutECTA), name: NSNotification.Name(rawValue: kLogout), object: nil)
        do{
            try reachability?.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    deinit {
        stopReachNotifier()
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func reachabilityChanged(_ sender: Notification) {
        let currentReachability: Reachability = sender.object as! Reachability
        checkReachability(currentReachability)
    }
    
    func checkReachability(_ reachability: Reachability) {
        if reachability.connection != .unavailable {
            if reachability.connection == .wifi {
                isReachable = true
                isReachableWiFi = true
            } else if reachability.connection == .cellular {
                isReachable = true
                isReachableWiFi = false
            }
        } else {
            isReachable = false
            isReachableWiFi = false
        }
    }
    
    func isUserAlreadyLogged(idUser: Int){
        idUserLogged = idUser
        isLoggedIn = true
    }
    
    func notify(_ userInfo: UserInfo, handler: ((UserInfo) -> Void )?) {
//        print("🔹 userInfo notified:", userInfo)
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kRepowerApiNotification), object: self, userInfo: userInfo)
            if let handler = handler { handler(userInfo) }
        }
    }
    
//    THIS FUNC FORCE THE CONNECTION WHEN SSL CERTIFICATE IS INVALID OR EXPIRED - USE IN DEV ONLY!
//    URLSESSION DELEGATE. REMOVE DELEGATE WHEN IN PRODUCTION!!
//    IN CASE OF NOT WORKING REMEMBER TO ENABLE Allow Arbitrary Loads in Info.plist!
//    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
//        print("API urlSession -> didReceive challenge URLAuthenticationChallenge")
//            completionHandler(.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
//    }
    
    // MARK: - Main function
    func call(_ method: String
        , httpMethod: HttpMethod = HttpMethod.GET
        , params: Params = [:]
        , contentType: String = "application/json; Charset=utf-8"
        , extraInfo: ExtraInfo? = nil
        , imageToUpload: Data? = nil
        , handler: ((UserInfo) -> Void )? = nil
        ) {
        
        var userInfo: UserInfo = ["success":false
            , "method": method
            , "date": Date()
            //, "data": nil
            //, "extraInfo": extraInfo
            //, "error": nil
        ]
        if let extraInfo = extraInfo { userInfo["extraInfo"] = extraInfo }
        
        let isConnectionOK = self.isConnectionReachable() // prima mostro il messaggio!
        
        guard isConnectionOK else {
            notify(userInfo, handler: handler)
            return
        }
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.requestCachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
       
//      **** NOTE: Use this configuration passing delegate to bypass errors when Invalid/Expired SSL Certificate
//      **** USE FOR DEV/DEBUG ONLY! - REMEMBER TO COMMENT THIS LINE WHEN IN PRODUCTION
            let session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
//      **** USE THIS LINE FOR PRODUCTION
//        let session = URLSession(configuration: sessionConfig)
        
        let paramsString = paramsToString(params)
        let paramsJson = paramsToJson(params)
        
        let urlPathComplete = !paramsString.isEmpty && (httpMethod == .GET || imageToUpload != nil) ? "\(kBasePath)\(method)\(paramsString)" : "\(kBasePath)\(method)"
        let url: URL = URL(string: urlPathComplete)!
        print("🔹🔹 API calling URL \(url) httpMethod \(httpMethod) paramsString \(paramsString)")
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 60.0)
        request.httpMethod = httpMethod.rawValue
        if (httpMethod == .POST) {
            if let imageToUpload = imageToUpload {
                request.httpBody = imageToUpload
            } else {
                request.addValue(contentType, forHTTPHeaderField: "Content-Type")
                request.httpBody = paramsJson
            }
        }
        
        // if not json
//        if contentType != "application/json; Charset=utf-8" {
//            request.addValue(contentType, forHTTPHeaderField: "Content-Type")
//        }
        
        func taskHandler (_ data: Data?, response: URLResponse?, error: Error?) {
//            print("taskHandler httpResponse  \(response.debugDescription)")
            
            
            let responseTask = ResponseTask(response: response, data: data, error: error)
            userInfo["date"] = Date()
            userInfo["data"] = data
            
            guard let data = responseTask.data, responseTask.error == nil else {
                print("🔴 httpResponse error = \(String(describing: error))")
                notify(userInfo, handler: handler)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else {
                print("🔴 httpResponse error: manca HTTPURLResponse")
                notify(userInfo, handler: handler)
                return
            }
            
            guard let httpResponseHeaders = httpResponse.allHeaderFields as? [String: Any]
                ,let httpResponseContentType = httpResponseHeaders["Content-Type"] as? String
                else {
                    print("🔴 httpResponse error No Content type for method", method)
                    notify(userInfo, handler: handler)
                    return
            }
            
//            print("****** httpResponseHeaders (\(method)) *******")
//            for httpResponseHeader in httpResponseHeaders {
//                print(httpResponseHeader.key, httpResponseHeader.value)
//            }
//            print("*************")
            
            let isHTTPStatusCodeOK = httpResponse.statusCode == 200
            if isHTTPStatusCodeOK {
                print("🔹 ", httpResponse.url ?? "", " --> statusCode ", httpResponse.statusCode)
            } else {
                print(httpResponse.url ?? "", " --> statusCode ", httpResponse.statusCode, " 🔴 httpResponse error 🔴")
            }
            
            userInfo["success"] = isHTTPStatusCodeOK
            
            switch httpResponseContentType.lowercased() {
            case let contentType where contentType.starts(with: "application/json"):
                if let jsonInfo = responseTask.jsonInfo()
                    , let json = jsonInfo["json"] as? [String: Any]
                {
                    userInfo["success"] = responseTask.checkJsonStatus(json)
                    userInfo.merge(jsonInfo) { (_, new) in new }
                }
            default: // image
                if let imageInfo = responseTask.imageInfo()
                    , isHTTPStatusCodeOK
                {
                    userInfo["success"] = true
                    userInfo.merge(imageInfo) { (_, new) in new }
                }
            }
            
            notify(userInfo, handler: handler)

            // Cache Data
            if let success = userInfo["success"] as? Bool, success {
                PersistenceController.shared.addJson(idUserLogged: Int32(idUserLogged), data: data, url: urlPathComplete, dataAggiornamento: Date())
            }
        }
        
        print("\n\(Date().description) 🔵 API call method: \(method) Type: \(httpMethod) Params: \(params)")
        if (httpMethod == .POST) {
            let task = session.uploadTask(with: request, from: paramsJson, completionHandler: taskHandler)
            task.resume()
        }else{
            let task = session.dataTask(with: url, completionHandler: taskHandler)
            task.resume()
        }
        
        
    }
    
    func goWithData(_ userInfo: UserInfo, handler: ((UserInfo) -> Void )?) {
        var userInfo = userInfo
        guard let data = userInfo["data"] as? Data else { return }
        let responseTask = ResponseTask(data: data)
        if let imageInfo = responseTask.imageInfo() {
            userInfo.merge(imageInfo) { (_, new) in new }
            api.notify(userInfo, handler: handler)
        }
    }
    
    func goWithJSON(_ userInfo: UserInfo, handler: ((UserInfo) -> Void )?) {
        var userInfo = userInfo
        guard let data = userInfo["data"] as? Data else { return }
        let responseTask = ResponseTask(data: data)
        if let imageInfo = responseTask.jsonInfo() {
            userInfo.merge(imageInfo) { (_, new) in new }
            api.notify(userInfo, handler: handler)
        }
    }
    
    func checkJsonCachedData(_ urlPathComplete: String) -> [Json]? {
        
        let fReq:NSFetchRequest<Json> = NSFetchRequest(entityName: "Json")
        
        let fPred1 = NSPredicate(format: "(url = %@)", urlPathComplete)
        //        print("idUserLogged = \(idUserLogged) isLoggedIn = \(isLoggedIn)")
        let fPred2 = NSPredicate(format: "(idUserLogged = %d)", idUserLogged)
        
        // Combine the two predicates above in to one compound predicate
        let fPred = NSCompoundPredicate(type: .and, subpredicates: [fPred1, fPred2])
        fReq.predicate = fPred
        
        let fSorter: NSSortDescriptor = NSSortDescriptor(key: "dateLastRetrive" , ascending: false)
        fReq.sortDescriptors = [fSorter]
        
        do {
//            if let fResults = try managedObjectContext.fetch(fReq) as? [Json] {
//                print("\(fResults.count) record in cache per \(urlPathComplete)")
//                return fResults
//            }
            let fResults = try managedObjectContext.fetch(fReq)
            print("\(fResults.count) record in cache per \(urlPathComplete)")
            return fResults
        }
        catch {
            print("checkJsonCachedData Error: \(error)")
        }
        
        return nil

    }
    
    func modifyHTTPHeaders(_ response: URLResponse, maxAge: Int) -> (URLResponse) {
        
        let HTTPResponse = response as! HTTPURLResponse
        let headers: Dictionary = HTTPResponse.allHeaderFields as Dictionary
        
        print(headers["Cache-Control"] ?? "no Cache-Control")
        
        var cachedResponse: URLResponse?
        if (headers["Cache-Control"] != nil) {
            //            var modifedHeaders = headers.mutableCopy() as! NSMutableDictionary
            //            modifedHeaders.setValue("max-age=\(maxAge)", forKey: "Cache-Control")
            
            let modifedHeaders = ["Cache-Control": "max-age=\(maxAge)"]
            let modifiedResponse = HTTPURLResponse(url: HTTPResponse.url!, statusCode: HTTPResponse.statusCode, httpVersion: "HTTP/1.1", headerFields: modifedHeaders)
            
            cachedResponse = modifiedResponse
        } else {
            cachedResponse = response
        }
        
        return cachedResponse!
    }
    
    func isConnectionReachable(_ showAlert: Bool = true) -> Bool {
        if showAlert && !isReachable && !isAlertMsgReachableShowing {
            //            self.showAlert("Sembra esserci qualche problema di connessione. Non posso continuare.")
            
            let alert = UIAlertController(title: "Attenzione", message: "Sembra esserci qualche problema di connessione. Non posso continuare.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            
            self.isAlertMsgReachableShowing = true
            
            UIApplication.shared.windows.first!.rootViewController?.present(alert, animated: true)
        
        }
        return isReachable
    }
    
    func jsonToString(_ json: Any) -> String? {
        do {
            let data =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            guard let convertedString = String(data: data, encoding: String.Encoding.utf8) else { return nil } // the data will be converted to the string
            return convertedString
        } catch let error {
            print(error)
            return nil
        }
        
    }
    
    func paramsToString(_ params: Params) -> String {
        
        var paramsStr: String = ""
        
        for (key, value) in params {
            let paramBit: String = String(format: "%@=%@", key, value )
            if(paramsStr != "") {
                paramsStr += "&"
            }
            paramsStr += paramBit
        }
        return paramsStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
    }
    
    func paramsToJson(_ params: Any) -> Data? {
        
        if !JSONSerialization.isValidJSONObject(params) {
            print("error: l'oggetto non è un json valido")
        }
        
        do {
            if let paramsJson = try JSONSerialization.data(withJSONObject: params, options: []) as Data? {
                let text: NSString = NSString(data: paramsJson, encoding: String.Encoding.utf8.rawValue)!
                print("paramsJson created -> \(text)")
                return paramsJson
            }
            return nil
        } catch {
            print("errore: \(error)")
            return nil
        }
    }
    
    func stopReachNotifier() {
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: nil)
        reachability = nil
    }
    
    
    func cleanAll() {
        print("Log Out & Cleaning all... ")
        appUtilities.cleanCookies()
        
//        _ = CoreDataController.shared.deleteCachedData("Json", onlyForIdUserLogged: true)
        
        //keychain
//        let removeIDSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "idConsulente")
//        let removeUsernameSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "username")
//        let removePwdSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "pwd")
//        let removeDateDSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "dateLastLogin")
//        print("removeIDSuccessful \(removeIDSuccessful)")
//        print("removeUsernameSuccessful \(removeUsernameSuccessful)")
//        print("removePwdSuccessful \(removePwdSuccessful)")
//        print("removeDateDSuccessful \(removeDateDSuccessful)")
//
        //keychain obsolete
//        let removeAuth_secretSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "auth_secret")
//        let removeDevice_uuidSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "device_uuid")
//        print("removeAuth_secretSuccessful \(removeAuth_secretSuccessful)")
//        print("removeDevice_uuidSuccessful \(removeDevice_uuidSuccessful)")
//
        isLoggedIn = false
        
        // evito background Fetch
//        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalNever)
    }
    
 
    
//    func goToLoginController(_ alertMessage: String? = nil) {
//        DispatchQueue.main.async {
//            if let window = UIApplication.shared.delegate?.window as? UIWindow, let loginVC = window.rootViewController?.storyboard?.instantiateViewController(withIdentifier: "Login") as? Landing {
//                window.rootViewController = loginVC
//
//                if let message = alertMessage {
//                    self.appUtilities.showAlert(message)
//                }
//            }
//        }
//    }
    
    func forceLogout(_ alertMessage:String? = nil) {
        cleanAll()
//        goToLoginController(alertMessage)
    }
}

// MARK: - ECTA ResponseTask class

class ResponseTask {
    var data: Data?
    var response: URLResponse?
    var error: Error?
    
    init(response: URLResponse? = nil, data: Data?, error: Error? = nil) {
        self.response = response
        self.data = data
        self.error = error
    }
    
    func jsonInfo() -> [String: Any]? {
        guard let data = data else { return nil }
        
        do {
            guard let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
                //error!
                if let dataString = String(data: data, encoding: .utf8) {
                    print("JSONSerialization Error:")
                    print(dataString.replacingOccurrences(of: "\\\"", with: "\""))
                }
                return nil
            }
            return ["json": json]
        }
        catch {
            print("Json data:", String(data: data, encoding: .utf8) ?? "")
            print("Json error", error)
            return ["error": error]
        }
    }
    
    func imageInfo() -> [String: UIImage]? {
        print("imageInfo:", data.debugDescription)
        guard let data = data, let image = UIImage(data: data) else { return nil }
        return ["image": image]
    }
    
}

extension ResponseTask {
    
    open func checkJsonStatus(_ json: [String: Any]) -> Bool{
        if let jsonStatus = json["status"] as? [String: Any] {
            return checkJsonStatusEcta(jsonStatus)
        }
        return false
    }
    
    private func checkJsonStatusEcta(_ jsonStatus: [String: Any]) -> Bool {
        let statusCode = jsonStatus["code"] as? String
        let statusDescription = jsonStatus["description"] as? String
        guard let userDescription = jsonStatus["userDescription"] as? String else { print("userDescription NOT FOUND!!!"); return  false}
        
        if let statusCode = statusCode, statusCode.hasPrefix("KO") {
            print("statusCode \(statusCode) -> \(String(describing: statusDescription))")
            if statusCode == "KO_AUTH " {
                api.forceLogout(statusDescription)
                print("--> KO_AUTH")
                
            } else {
                if let statusDescription = statusDescription, !statusDescription.isEmpty {
                    api.appUtilities.showAlert(userDescription, titleMessage: statusDescription, titleCancelButton: "Cancella" )
                    print("--> KO_AUTH ERROR ", statusDescription)
                }
            }
            return false
        }
        if statusCode == "OK_OLD_VERSION" {
            if let statusDescription = statusDescription, !statusDescription.isEmpty {
                api.appUtilities.showAlert(userDescription, titleMessage: statusDescription, titleCancelButton: "Cancella")
                print("--> OK_OLD_VERSION ", statusDescription)
            }
        }
        return true
    }
}

