//
//  ListView.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 04/12/2020.
//
import Foundation
import SwiftUI

struct ListView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @EnvironmentObject var consulenti: StrutturaWrapper

   
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
            }) {
                HStack {
                    Text("Indietro") //new Back button title
                }
            }
    }
    
    var body: some View {
        
        ZStack {
            VStack {
                    List(consulenti.struttura!.consulenti){ consulente in
                        ConsulenteCell(consulente: consulente)
                    }
                    .edgesIgnoringSafeArea(.all)
                    .navigationBarTitle("Struttura")
                    .onReceive(NotificationCenter.default.publisher(for: Notification.Name(rawValue: kRepowerApiNotification))) { value in
                            print("🔹🔹🔹 Notification received In LIST!! 🔹🔹🔹")
                            print("--> \(value)")
                            let usrDict: Dictionary = value.userInfo!
                            print("method --> \(usrDict["method"] as! String)")
                            if (usrDict["method"] as! String == "struttura" )  {
                                print("🔹 GOT STRUTTURA IN LIST !!!! 🔹")
                            }
                        }
            }
        }.onAppear {
            if (consulenti.struttura != nil) {
                print("Struttura OK! ")
            }else{
                print("Azzz... Struttura e' nil! ")
            }
        }
    }
}

struct ConsulenteCell: View {
    
    var consulente: Consulente
    
    var body: some View {
        ZStack {
            HStack {
                Image("struttura_face_4")
//                    .frame(width: 50, height: 50)
                
                VStack(alignment: .leading) {
                    Text(consulente.cognome + " " + consulente.nome)
                    let ss: String = "clienti " + String(consulente.clienti!) + " - prenotazioni " + String(consulente.prenotazioni!) + "/" + String(consulente.limite_prenotazioni!)
                    Text(ss).font(.subheadline).foregroundColor(.red)
                }
                
                Spacer()
                
            }
        }.onTapGesture {
            
            print("CIAUU")
        }
    }
}


struct ListView_Previews: PreviewProvider {
    
    static var previews: some View {
        ListView()
        
    }
}
