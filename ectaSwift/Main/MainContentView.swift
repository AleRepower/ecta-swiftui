//
//  MainContentView.swift
//  ectaSwift
//
//  Created by Alessandro Grigiante on 03/11/21.
//  Copyright © 2021 REPower SpA. All rights reserved.
//

import SwiftUI

struct MainContentView: View {
    @State var selectedItem = menus[1]
    @State var showSideMenu = false
    
    var body: some View {
        ZStack {
            SideMenuView(selectedItem: self.$selectedItem, showSideMenu: self.$showSideMenu)
            MainView(selectedItem: self.selectedItem, showSideMenu: self.$showSideMenu)
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .frame(maxWidth: .infinity, alignment: .topLeading)
        .edgesIgnoringSafeArea(.all)
    }
}

struct MainContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainContentView()
    }
}
